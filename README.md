## Ágora 2030 - API

**Requisitos**

 - nodejs >= 4.0.0
 - mysql >= 5.0

Faça o clone do projeto e instale as dependências

> git clone https://gitlab.com/agora2030/api.git

> cd api

> npm install

**Configuração**

No Mysql crie um banco 'agoradb'

Vá na pasta "config/env" e faça uma cópia do conteúdo do arquivo development.sample e crie um novo arquivo 'development.js' na mesma pasta com o conteúdo copiado.

No seu arquivo 'development.js' altere as seguintes configurações:
    
    sequelize: {
	    database: 'agoradb',
	    user: 'usuario_do_mysql',
	    password: 'senha_do_mysql'
    }

**Seed**
	
Para rodar o seed execute: `node data/db/seed.js`
ps.: O seed não apagará o conteúdo ja existente no banco

**Executar**

Para startar a api execute `npm run dev` por padrão a api responderá em: `http://localhost:3000/v1`

**Documentação**
 
Após executar o serviço a documentação das rotas estará disponível no link: `http://localhost:3000/docs`

**Testes**

Para executar os testes, crie um novo banco em seu postgresql 'agoradbTest' e atualize as configurações do banco no arquivo: `config/env/test.js` após execute: `npm test` para executar os testes automatizados.
