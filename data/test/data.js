module.exports = {
  initiatives: [{
    title: 'Tratamento Teste',
    description: 'Relato de tratamento inovador',
    institutionName: 'FIOCRUZ',
    contactName: 'Ezio Zouza',
    contactEmail: 'ezio@mail.com',
    language: 'Português',
    addressFormatted: 'Rua Sergio 12',
    addressLatitude: 1.2923423423,
    addressLongitude: -1.21223321,
    addressCity: 'Brasília',
    addressCountry: 'Brasil',
    addressState: 'Distrito Federal',
    tags: ['Inovação', 'Tratamento'],
    published: true
  },
  {
    title: 'Combate a fome',
    description: 'Relato de tratamento inovador',
    institutionName: 'FIOCRUZ',
    contactName: 'Ezio Zouza',
    contactEmail: 'ezio@mail.com',
    language: 'Português',
    addressFormatted: 'Rua Sergio 12',
    addressLatitude: 1.2923423423,
    addressLongitude: -1.21223321,
    addressCountry: 'Brasil',
    addressCity: 'Brasília',
    addressState: 'Distrito Federal',
    tags: ['Combate', 'Pesquisa']
  }],
  users: [
    {
      firstName: 'User Admin',
      lastName: 'Rede',
      email: 'admin@mail.com',
      password: '123456',
      country: 'Brasil',
      state: 'Paraíba',
      city: 'Campina Grande',
      role: 'admin',
      xp: 100,
      gender: 'Masculino'
    },
    {
      firstName: 'User Normal',
      lastName: 'Rede',
      email: 'user@mail.com',
      password: '123456',
      country: 'Brasil',
      state: 'São Paulo',
      city: 'São José dos Campos',
      role: 'user',
      xp: 150,
      favorites: 1,
      gender: 'Masculino'
    },
    {
      firstName: 'Juliana',
      lastName: 'Silva',
      email: 'juliana@gmail.com',
      password: '123456',
      country: 'Brasil',
      state: 'São Paulo',
      city: 'São José dos Campos',
      role: 'user',
      gender: 'Feminino'
    },
    {
      firstName: 'String',
      lastName: 'Danadão',
      email: 'stringaodanadao@gmail.com',
      country: 'Brasil',
      state: 'Paraíba',
      city: 'Campina Grande',
      role: 'user',
      gender: 'Masculino'
    }
  ]
}
