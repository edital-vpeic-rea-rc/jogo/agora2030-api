'use strict'

const models = require('../../src/models')
const data = require('./data')

function init () {
  console.log('--> Init Seed')
  models.sequelize.sync()
  .then(createOds)
  .then(createMissions)
  .then(finish => {
    console.log('--> Seed finished')
    process.exit()
  })
  .catch(err => {
    console.log('Seed error: ', err)
    process.exit()
  })
}

function createOds () {
  return models.Ods.bulkCreate(require('../util/ods'), {individualHooks: true})
}

function createMissions () {
  return models.Mission.bulkCreate(require('../util/missions'), {individualHooks: true})
}

init()
