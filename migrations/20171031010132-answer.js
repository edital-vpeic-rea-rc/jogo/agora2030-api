'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable(
      'answers', {
        id: {
          type: Sequelize.UUID,
          defaultValue: Sequelize.UUIDV4,
          primaryKey: true
        },

        text: {
          type: Sequelize.TEXT('long'),
          allowNull: false
        },

        initiativeId: {
          type: Sequelize.UUID,
          allowNull: false
        },

        questionId: {
          type: Sequelize.UUID,
          allowNull: false
        },

        createdAt: {
          type: Sequelize.DATE,
          allowNull: false
        },

        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false
        }
      }
    )
  },
  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('answers')
  }
}
