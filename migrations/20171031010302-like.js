'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable(
      'likes', {
        id: {
          type: Sequelize.UUID,
          defaultValue: Sequelize.UUIDV4,
          primaryKey: true
        },

        terms: {
          type: Sequelize.STRING,
          get: function () {
            return this.getDataValue('terms') ? this.getDataValue('terms').split(';') : ''
          },
          set: function (val) {
            this.setDataValue('terms', val.join(';'))
          }
        },

        userId: {
          type: Sequelize.UUID,
          allowNull: false
        },

        initiativeId: {
          type: Sequelize.UUID
        },

        createdAt: {
          type: Sequelize.DATE,
          allowNull: false
        },

        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false
        },

        deletedAt: {
          type: Sequelize.DATE
        }
      }
    )
  },
  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('likes')
  }
}
