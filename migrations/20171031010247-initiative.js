'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable(
      'initiatives', {
        id: {
          type: Sequelize.UUID,
          defaultValue: Sequelize.UUIDV4,
          primaryKey: true
        },

        title: {
          type: Sequelize.STRING,
          allowNull: false
        },

        description: {
          type: Sequelize.TEXT('medium'),
          allowNull: false
        },

        institutionName: {
          type: Sequelize.STRING,
          allowNull: false
        },

        contactName: {
          type: Sequelize.STRING,
          allowNull: false
        },

        contactEmail: {
          type: Sequelize.STRING,
          allowNull: false,
          validate: {
            isEmail: true
          }
        },

        source: {
          type: Sequelize.STRING
        },

        language: {
          type: Sequelize.STRING,
          allowNull: false
        },

        addressCountry: {
          type: Sequelize.STRING
        },

        addressState: {
          type: Sequelize.STRING
        },

        addressCity: {
          type: Sequelize.STRING
        },

        addressFormatted: {
          type: Sequelize.STRING,
          allowNull: false
        },

        addressLatitude: {
          type: Sequelize.DOUBLE,
          allowNull: false
        },

        addressLongitude: {
          type: Sequelize.DOUBLE,
          allowNull: false
        },

        published: {
          type: Sequelize.BOOLEAN,
          defaultValue: false
        },

        publishedAt: {
          type: Sequelize.DATE
        },

        slug: {
          type: Sequelize.STRING,
          trim: true,
          unique: { msg: 'Título already exists' }
        },

        createdAt: {
          type: Sequelize.DATE,
          allowNull: false
        },

        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false
        },

        deletedAt: {
          type: Sequelize.DATE
        }
      }
    )
  },
  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('initiatives')
  }
}
