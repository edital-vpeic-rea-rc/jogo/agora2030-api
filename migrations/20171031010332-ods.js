'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable(
      'ods', {
        id: {
          type: Sequelize.UUID,
          defaultValue: Sequelize.UUIDV4,
          primaryKey: true
        },

        number: {
          type: Sequelize.INTEGER,
          allowNull: false
        },

        title: {
          type: Sequelize.STRING,
          allowNull: false
        },

        description: {
          type: Sequelize.STRING,
          allowNull: false
        },

        color: {
          type: Sequelize.STRING,
          allowNull: false
        },

        active: {
          type: Sequelize.BOOLEAN,
          defaultValue: true
        },

        createdAt: {
          type: Sequelize.DATE,
          allowNull: false
        },

        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false
        },

        deletedAt: {
          type: Sequelize.DATE
        }
      }
    )
  },
  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('ods')
  }
}
