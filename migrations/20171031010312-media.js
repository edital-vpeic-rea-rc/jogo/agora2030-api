'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable(
      'medias', {
        id: {
          type: Sequelize.UUID,
          defaultValue: Sequelize.UUIDV4,
          primaryKey: true
        },

        originalFileName: {
          type: Sequelize.STRING,
          allowNull: false
        },

        extName: {
          type: Sequelize.STRING,
          allowNull: false
        },

        contentType: {
          type: Sequelize.STRING,
          allowNull: false
        },

        mediaType: {
          type: Sequelize.ENUM('image', 'file'),
          allowNull: false
        },

        fileName: {
          type: Sequelize.STRING,
          allowNull: false
        },

        folderPath: {
          type: Sequelize.STRING,
          allowNull: false
        },

        caption: {
          type: Sequelize.STRING
        },

        initiativeId: {
          type: Sequelize.UUID,
          allowNull: false
        },

        coverImageId: {
          type: Sequelize.UUID
        },

        createdAt: {
          type: Sequelize.DATE,
          allowNull: false
        },

        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false
        }
      }
    )
  },
  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('medias')
  }
}
