const nodemailer = require('nodemailer')
const config = require('./env')
const smtpTransport = require('nodemailer-smtp-transport')

if (config.env === 'development') {
  exports.transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: config.nodemailer.email,
      pass: config.nodemailer.password
    }
  })
} else if (config.env === 'production') {
  exports.transporter = nodemailer.createTransport(smtpTransport(config.nodemailer))
}
