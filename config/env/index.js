'use strict'

const env = process.env.NODE_ENV || 'development'
const config = require(`./${env}`)

const defaults = {
  fileSize: 10000000,
  facebookUrlCheckToken: 'https://graph.facebook.com/debug_token',
  googleUrlCheckToken: 'https://www.googleapis.com/oauth2/v3/tokeninfo',
  imageStyles: [
    {
      name: '150x150',
      width: 150,
      heigth: 150
    },
    {
      name: '240x260',
      width: 240,
      heigth: 260
    },
    {
      name: '570x370',
      width: 570,
      heigth: 370
    }
  ],
  avatarStyles: [
    {
      name: '150x150',
      width: 150,
      heigth: 150
    },
    {
      name: '240x260',
      width: 240,
      heigth: 260
    }
  ],
  acceptMimes: [
    'image/x-png', 'image/png', 'image/gif', 'image/jpeg',
    'application/pdf', 'text/plain', 'application/msword',
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    'application/excel', 'application/vnd.ms-excel', 'application/msexcel',
    'application/powerpoint', 'application/vnd.ms-powerpoint', 'application/octet-stream',
    'text/x-comma-separated-values', 'text/comma-separated-values',
    'application/vnd.oasis.opendocument.text', 'text/csv',
    'application/vnd.openxmlformats-officedocument.presentationml.presentation'
  ]
}

module.exports = Object.assign(defaults, config)
