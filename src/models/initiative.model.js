'use strict'
const slugify = require('underscore.string/slugify')

module.exports = (sequelize, DataTypes) => {
  const Initiative = sequelize.define('Initiative', {

    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },

    title: {
      type: DataTypes.STRING,
      allowNull: false
    },

    description: {
      type: DataTypes.TEXT('medium'),
      allowNull: false
    },

    institutionName: {
      type: DataTypes.STRING,
      allowNull: false
    },

    contactName: {
      type: DataTypes.STRING,
      allowNull: false
    },

    contactEmail: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        isEmail: true
      }
    },

    source: {
      type: DataTypes.STRING
    },

    language: {
      type: DataTypes.STRING,
      allowNull: false
    },

    addressCountry: {
      type: DataTypes.STRING
    },

    addressState: {
      type: DataTypes.STRING
    },

    addressCity: {
      type: DataTypes.STRING
    },

    addressFormatted: {
      type: DataTypes.STRING,
      allowNull: false
    },

    addressLatitude: {
      type: DataTypes.DOUBLE,
      allowNull: false
    },

    addressLongitude: {
      type: DataTypes.DOUBLE,
      allowNull: false
    },

    published: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },

    publishedAt: {
      type: DataTypes.DATE
    },

    slug: {
      type: DataTypes.STRING,
      trim: true,
      unique: { msg: 'Título already exists' }
    }
  }, {
    tableName: 'initiatives',
    hooks: {
      beforeCreate: function (initiative) {
        if (initiative.title) {
          initiative.slug = slugify(initiative.title)
        }
      },
      beforeUpdate: function (initiative) {
        if (initiative.changed('title')) {
          initiative.slug = slugify(initiative.title)
        }
      }
    }
  })

  Initiative.associate = (models) => {
    Initiative.belongsToMany(models.Tag, {as: 'tags', through: models.InitiativeTags, foreignKey: 'initiativeId'})
    Initiative.belongsToMany(models.Ods, {as: 'ods', through: models.InitiativeOds, foreignKey: 'initiativeId'})
    Initiative.hasOne(models.Media, {as: 'coverImage', foreignKey: {field: 'coverImageId'}})
    Initiative.hasMany(models.Trace, {as: 'trace', foreignKey: {field: 'initiativeId', constraints: false}})
  }

  Initiative.prototype.toJSON = function () {
    let values = Object.assign({}, this.get())
    return values
  }

  return Initiative
}
