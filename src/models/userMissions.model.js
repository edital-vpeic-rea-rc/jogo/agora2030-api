'use strict'

module.exports = (sequelize, DataTypes) => {
  const UserMissions = sequelize.define('UserMissions', {

    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },

    missionId: {
      type: DataTypes.UUID,
      allowNull: false
    },

    userId: {
      type: DataTypes.UUID,
      allowNull: false
    }

  }, {
    tableName: 'user_missions',
    paranoid: false
  })

  return UserMissions
}
