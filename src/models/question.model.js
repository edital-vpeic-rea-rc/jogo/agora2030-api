'use strict'

module.exports = (sequelize, DataTypes) => {
  const Question = sequelize.define('Question', {

    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },

    title: {
      type: DataTypes.TEXT('medium'),
      allowNull: false
    },

    help: {
      type: DataTypes.TEXT('medium')
    },

    active: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },

    mobile: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },

    order: {
      type: DataTypes.INTEGER
    }

  }, {
    tableName: 'questions'
  })

  return Question
}
