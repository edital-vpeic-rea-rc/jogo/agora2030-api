'use strict'

module.exports = (sequelize, DataTypes) => {
  const InitiativeOds = sequelize.define('InitiativeOds', {

    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },

    initiativeId: {
      type: DataTypes.UUID,
      allowNull: false
    },

    odsId: {
      type: DataTypes.UUID,
      allowNull: false
    }

  }, {
    tableName: 'initiative_ods',
    paranoid: false
  })

  return InitiativeOds
}
