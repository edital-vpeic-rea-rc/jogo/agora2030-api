'use strict'

const fs = require('fs')
const path = require('path')
const Sequelize = require('sequelize')
const config = require('../../config/env')

let sequelize

if (config.sequelize.databaseUrl !== '') {
  sequelize = new Sequelize(config.sequelize.databaseUrl, config.sequelize.params)
} else {
  sequelize = new Sequelize(
    config.sequelize.database,
    config.sequelize.user,
    config.sequelize.password,
    config.sequelize.params
  )
}

let db = {}

const basename = path.basename(module.filename)

fs
  .readdirSync(__dirname)
  .filter(file =>
    (file.indexOf('.') !== 0) &&
    (file !== basename) &&
    (file.slice(-3) === '.js'))
  .forEach(file => {
    const model = sequelize.import(path.join(__dirname, file))
    db[model.name] = model
  })

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db)
  }
})

db.sequelize = sequelize
db.Sequelize = Sequelize

module.exports = db
