'use strict'
const bcrypt = require('bcrypt')
const Helpers = require('../core/lib/helpers')
const config = require('../../config/env')

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {

    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },

    role: {
      type: DataTypes.ENUM('admin', 'user'),
      defaultValue: 'user'
    },

    firstName: {
      type: DataTypes.STRING,
      allowNull: false
    },

    lastName: {
      type: DataTypes.STRING,
      allowNull: false
    },

    email: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false,
      validate: {
        isEmail: true
      }
    },

    xp: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },

    coins: {
      type: DataTypes.INTEGER,
      defaultValue: 100
    },

    favorites: {
      type: DataTypes.INTEGER,
      defaultValue: 10
    },

    password: {
      type: DataTypes.STRING
    },

    country: {
      type: DataTypes.STRING
    },

    state: {
      type: DataTypes.STRING
    },

    city: {
      type: DataTypes.STRING
    },

    gender: {
      type: DataTypes.ENUM('Masculino', 'Feminino')
    },

    major: {
      type: DataTypes.STRING
    },

    occupation: {
      type: DataTypes.STRING
    },

    phoneNumber: {
      type: DataTypes.STRING
    },

    socialLogin: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },

    socialLoginOrigins: {
      type: DataTypes.STRING,
      get: function () {
        return this.getDataValue('socialLoginOrigins') ? this.getDataValue('socialLoginOrigins').split(';') : ''
      },
      set: function (val) {
        this.setDataValue('socialLoginOrigins', val.join(';'))
      }
    },

    socialAvatar: {
      type: DataTypes.STRING
    },

    avatarPath: {
      type: DataTypes.STRING
    },

    avatarExtName: {
      type: DataTypes.STRING
    },

    receiveEmail: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },

    receivePush: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },

    emailConfirmationToken: {
      type: DataTypes.STRING
    },

    emailConfirmationSendAt: {
      type: DataTypes.DATE
    },

    emailConfirmedAt: {
      type: DataTypes.DATE
    },

    recoveryPasswordToken: {
      type: DataTypes.STRING
    },

    solRecoveryPasswordAt: {
      type: DataTypes.DATE
    },

    recoveryPasswordAt: {
      type: DataTypes.DATE
    }

  }, {
    tableName: 'users',
    scopes: {
      public: () => {
        return {
          order: [['missions', 'order', 'ASC']],
          attributes: [
            'id', 'firstName', 'lastName', 'state',
            'city', 'major', 'occupation', 'xp', 'avatarPath',
            'socialAvatar', 'avatarExtName', 'coins', 'favorites'
          ],
          include: [
            {
              attributes: ['title', 'order'],
              association: 'missions',
              through: {attributes: []}
            }
          ]
        }
      },
      me: () => {
        return {
          order: [['missions', 'order', 'ASC']],
          include: [
            {
              attributes: ['title', 'order'],
              association: 'missions',
              through: {attributes: []}
            }
          ]
        }
      },
      list: {
        attributes: [
          'id', 'firstName', 'lastName', 'xp', 'avatarPath',
          'socialAvatar', 'avatarExtName', 'createdAt'
        ]
      },
      resume: {
        attributes: [
          'id', 'firstName', 'lastName', 'xp', 'coins', 'role', 'avatarPath',
          'socialAvatar', 'avatarExtName', 'favorites'
        ]
      },
      missions: () => {
        return {
          attributes: [],
          order: [['missions', 'order', 'ASC']],
          include: [
            {
              attributes: ['title', 'order'],
              association: 'missions',
              through: {attributes: []}
            }
          ]
        }
      }
    },
    hooks: {
      beforeCreate: function (user) {
        if (user.password) {
          user.password = hashPassword(user.password)
        }
      },
      beforeUpdate: function (user) {
        if (user.changed('password')) {
          user.password = hashPassword(user.password)
        }
      }
    }
  })

  User.associate = (models) => {
    User.belongsToMany(models.Mission, {as: 'missions', through: models.UserMissions, foreignKey: 'userId'})
    User.hasMany(models.Trace, {as: 'trace', foreignKey: {field: 'userId'}})
  }

  User.prototype.validatePassword = function (password) {
    return bcrypt.compareSync(password, this.get('password'))
  }

  User.prototype.userLevel = function userLevel () {
    const level = Helpers.userLevel(this.get('xp'))
    return level
  }

  User.prototype.toJSON = function () {
    let values = Object.assign({}, this.get())

    values.nivel = this.userLevel()
    values.avatarUrls = null

    if (values.avatarPath) {
      const folder = config.staticUrl + values.avatarPath
      values.avatarUrls = {}
      values.avatarUrls.original = `${folder}original${values.avatarExtName}`
      config.avatarStyles.forEach(style => {
        values.avatarUrls[style.name] = `${folder}${style.name}${values.avatarExtName}`
      })
    }

    delete values.password
    delete values.emailConfirmationToken
    delete values.emailConfirmationSendAt
    delete values.emailConfirmedAt
    delete values.recoveryPasswordToken
    delete values.solRecoveryPasswordAt
    delete values.recoveryPasswordAt
    delete values.avatarPath
    delete values.avatarExtName
    return values
  }

  return User
}

function hashPassword (password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(8))
}
