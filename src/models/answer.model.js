'use strict'

module.exports = (sequelize, DataTypes) => {
  const Answer = sequelize.define('Answer', {

    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },

    text: {
      type: DataTypes.TEXT('long'),
      allowNull: false
    }

  }, {
    tableName: 'answers',
    paranoid: false
  })

  Answer.associate = (models) => {
    Answer.belongsTo(models.Question, {as: 'question', foreignKey: {field: 'questionId', unique: 'unique_answer', allowNull: false}})
    Answer.belongsTo(models.Initiative, {as: 'initiative', foreignKey: {field: 'initiativeId', unique: 'unique_answer', allowNull: false}})
  }

  return Answer
}
