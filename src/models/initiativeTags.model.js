'use strict'

module.exports = (sequelize, DataTypes) => {
  const InitiativeTags = sequelize.define('InitiativeTags', {

    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },

    initiativeId: {
      type: DataTypes.UUID,
      allowNull: false
    },

    tagId: {
      type: DataTypes.UUID,
      allowNull: false
    }

  }, {
    tableName: 'initiative_tags',
    paranoid: false
  })

  return InitiativeTags
}
