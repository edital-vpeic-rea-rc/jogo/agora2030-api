'use strict'

module.exports = (sequelize, DataTypes) => {
  const Donate = sequelize.define('Donate', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },
    value: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, {
    tableName: 'donates'
  })

  Donate.associate = (models) => {
    Donate.belongsTo(models.User, {as: 'user', foreignKey: {field: 'userId', unique: 'unique_donate', allowNull: false}})
    Donate.belongsTo(models.Initiative, {as: 'initiative', foreignKey: {field: 'initiativeId', unique: 'unique_donate', allowNull: false}})
  }

  return Donate
}
