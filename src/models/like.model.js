'use strict'

module.exports = (sequelize, DataTypes) => {
  const Like = sequelize.define('Like', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },
    terms: {
      type: DataTypes.STRING,
      get: function () {
        return this.getDataValue('terms') ? this.getDataValue('terms').split(';') : ''
      },
      set: function (val) {
        this.setDataValue('terms', val.join(';'))
      }
    }
  }, {
    tableName: 'likes'
  })

  Like.associate = (models) => {
    Like.belongsTo(models.User, {as: 'user', foreignKey: {field: 'userId', unique: 'unique_like', allowNull: false}})
    Like.belongsTo(models.Initiative, {as: 'initiative', foreignKey: {field: 'initiativeId', unique: 'unique_like', allowNull: false}})
  }

  return Like
}
