'use strict'

module.exports = (sequelize, DataTypes) => {
  const Mission = sequelize.define('Mission', {

    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },

    title: {
      type: DataTypes.STRING,
      allowNull: false
    },

    description: {
      type: DataTypes.TEXT('long'),
      allowNull: false
    },

    order: {
      type: DataTypes.INTEGER,
      allowNull: false
    },

    actionLike: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },

    actionView: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },

    actionShare: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },

    actionDonate: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },

    actionFavorite: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },

    actionComment: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    }

  }, {
    tableName: 'missions'
  })

  Mission.associate = (models) => {
    Mission.belongsToMany(models.User, {as: 'users', through: models.UserMissions, foreignKey: 'missionId'})
  }

  Mission.prototype.toJSON = function () {
    let values = Object.assign({}, this.get())
    return values
  }

  return Mission
}
