'use strict'

module.exports = (sequelize, DataTypes) => {
  const Comment = sequelize.define('Comment', {

    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },

    text: {
      type: DataTypes.TEXT('long'),
      allowNull: false
    }

  }, {
    tableName: 'comments',
    paranoid: false
  })

  Comment.associate = (models) => {
    Comment.belongsTo(models.User, {as: 'user', foreignKey: {field: 'userId', allowNull: false}})
    Comment.belongsTo(models.Initiative, {as: 'initiative', foreignKey: {field: 'initiativeId'}})
  }

  Comment.prototype.toJSON = function () {
    let values = Object.assign({}, this.get())
    return values
  }

  return Comment
}
