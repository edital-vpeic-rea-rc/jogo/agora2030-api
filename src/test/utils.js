'use strict'

const Promise = require('bluebird')
const models = require('../models')

module.exports = {
  getToken, getMission, getInitiative, getUser, getInitiatives
}

function getToken (server, role = 'user') {
  return new Promise((resolve, reject) => {
    const payload = {
      email: 'user@mail.com',
      password: '123456'
    }

    if (role === 'admin') {
      payload.email = 'admin@mail.com'
    }

    const options = {
      method: 'POST',
      url: '/v1/login',
      payload: payload
    }

    server.inject(options, (response) => {
      resolve(response.result)
    })
  })
}

function getMission () {
  return models.Mission.findAndCountAll()
  .then(result => {
    return result.rows[0].id
  })
}

function getInitiative () {
  return models.Initiative.findAndCountAll()
  .then(result => {
    return result.rows[0].id
  })
}

function getInitiatives () {
  return models.Initiative.findAndCountAll()
  .then(result => {
    return result.rows
  })
}
function getUser (role = 'user') {
  const email = role === 'admin' ? 'admin@mail.com' : 'user@mail.com'
  return models.User.findOne({
    where: {
      email: email
    }
  })
}
