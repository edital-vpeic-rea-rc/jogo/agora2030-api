const handlebars = require('handlebars')
const templates = require('./templates')
const subjects = require('./templates/subject')
const transporter = require('../../config/nodemailer').transporter
const config = require('../../config/env')
const Promise = require('bluebird')

exports.sendMail = (options) => {
  return new Promise((resolve, reject) => {
    if (!options.data.email) {
      return reject(new Error('email is required'))
    }

    if (!options.template) {
      return reject(new Error('template is required'))
    }

    if (!templates[options.template]) {
      return reject(new Error('template not found'))
    }

    const template = handlebars.compile(templates[options.template])
    const html = template(options.data)

    const mailOptions = {
      to: options.data.email,
      subject: subjects[options.template],
      html: html
    }

    if (config.env === 'production') {
      mailOptions.from = `${config.fromName} <${config.siteEmail}>`
    }

    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        console.log('Error on send email: ', error)
        return reject(error)
      } else {
        resolve({message: 'OK'})
      }
    })
  })
}
