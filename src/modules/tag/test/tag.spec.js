/* global describe, before, it, expect, server */

const utils = require('../../../test/utils')
const ENDPOINT = '/v1/tags'

describe(`#Tag`, () => {
  let userToken = null
  let adminToken = null
  let id = null

  before((done) => {
    utils.getToken(server)
    .then((userContext) => {
      userToken = userContext.token
      utils.getToken(server, 'admin')
      .then((adminContext) => {
        adminToken = adminContext.token
        done()
      })
    })
  })

  describe(`POST ${ENDPOINT}`, () => {
    it('create a new', (done) => {
      const options = {
        method: 'POST',
        url: ENDPOINT,
        headers: {'Authorization': `Bearer ${adminToken}`},
        payload: {
          text: 'saude'
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        expect(response.result).to.exist()
        expect(response.result.id).to.exist()
        expect(response.result.text).to.exist()
        id = response.result.id
        done()
      })
    })

    it('403 - role', (done) => {
      const options = {
        method: 'POST',
        url: ENDPOINT,
        headers: {'Authorization': `Bearer ${userToken}`},
        payload: {
          text: 'saude'
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(403)
        done()
      })
    })

    it('not send required data', (done) => {
      const options = {
        method: 'POST',
        url: ENDPOINT,
        headers: {'Authorization': `Bearer ${adminToken}`},
        payload: {}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(400)
        done()
      })
    })
  })

  describe(`GET ${ENDPOINT}`, () => {
    it('get all', (done) => {
      const options = {
        method: 'GET',
        url: ENDPOINT,
        headers: {'Authorization': `Bearer ${adminToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        expect(response.result).to.exist()
        expect(response.result.total).to.exist()
        expect(response.result.skip).to.exist()
        expect(response.result.limit).to.exist()
        expect(response.result.items).to.not.be.empty()
        done()
      })
    })
    it('filter', (done) => {
      const options = {
        method: 'GET',
        url: `${ENDPOINT}?text=sau`,
        headers: {'Authorization': `Bearer ${adminToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        expect(response.result).to.exist()
        expect(response.result.total).to.equals(1)
        expect(response.result.skip).to.exist()
        expect(response.result.limit).to.exist()
        expect(response.result.items).to.not.be.empty()
        done()
      })
    })
    it('filter not exist', (done) => {
      const options = {
        method: 'GET',
        url: `${ENDPOINT}?text=not`,
        headers: {'Authorization': `Bearer ${adminToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        expect(response.result).to.exist()
        expect(response.result.total).to.equals(0)
        expect(response.result.skip).to.exist()
        expect(response.result.limit).to.exist()
        expect(response.result.items).to.be.empty()
        done()
      })
    })
  })

  describe(`GET ${ENDPOINT}/{id}`, () => {
    it('get one', (done) => {
      const options = {
        method: 'GET',
        url: `${ENDPOINT}/${id}`,
        headers: {'Authorization': `Bearer ${userToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        expect(response.result).to.exist()
        expect(response.result.id).to.equals(id)
        done()
      })
    })

    it('not found', (done) => {
      const options = {
        method: 'GET',
        url: `${ENDPOINT}/09919733-3d2f-4d7a-a9bd-ad6d4c3822e7`,
        headers: {'Authorization': `Bearer ${userToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(404)
        done()
      })
    })

    it('invalid id', (done) => {
      const options = {
        method: 'GET',
        url: `${ENDPOINT}/invalid`,
        headers: {'Authorization': `Bearer ${userToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(400)
        done()
      })
    })
  })
})
