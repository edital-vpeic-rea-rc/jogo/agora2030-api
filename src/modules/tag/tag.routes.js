'use strict'

const Handler = require('./tag.handler')
const Schema = require('./tag.schema')
const Helpers = require('../../core/lib/helpers')

module.exports = [
  {
    method: 'GET',
    path: '/tags',
    config: {
      description: 'GET tags',
      notes: 'Get all tags',
      tags: ['api', 'tags'],
      auth: {
        scope: ['admin', 'user']
      },
      handler: Handler.list,
      validate: {
        query: Schema.query
      },
      response: {
        schema: Helpers.Joi.validate.pagination.response
      }
    }
  },
  {
    method: 'POST',
    path: '/tags',
    config: {
      description: 'create one tags',
      tags: ['api', 'tags'],
      auth: {
        scope: ['admin']
      },
      handler: Handler.create,
      validate: {
        payload: Schema.create
      }
    }
  },
  {
    method: 'GET',
    path: '/tags/{id}',
    config: {
      description: 'GET one tags',
      notes: 'Get one tags by id',
      tags: ['api', 'user'],
      auth: {
        scope: ['admin', 'user']
      },
      handler: Handler.read,
      validate: {
        params: {id: Helpers.Joi.validate.uuidv4}
      }
    }
  }
]
