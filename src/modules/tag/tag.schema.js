'use strict'

const Joi = require('joi')
const Helpers = require('../../core/lib/helpers')

const schema = {
  text: Joi.string().max(20)
}

const create = Joi.object({
  text: schema.text.required()
})

const query = Helpers.Joi.validate.pagination.query.concat(Joi.object({
  text: Joi.string().max(20)
}))

module.exports = {create, query}
