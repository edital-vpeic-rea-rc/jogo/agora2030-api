'use strict'

const models = require('../../models')
const Op = models.Sequelize.Op

module.exports = {
  list, create, read
}

async function list (request, reply) {
  try {
    const searchText = request.query.text
    const options = {
      limit: request.query.limit,
      offset: request.query.skip
    }
    if (searchText) {
      options.where = {
        text: {
          [Op.like]: `%${searchText.toLowerCase()}%`
        }
      }
    }
    const result = await models.Tag.findAndCountAll(options)
    const response = {
      total: result.count,
      skip: request.query.skip,
      limit: request.query.limit,
      items: result.rows
    }
    reply(response)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function create (request, reply) {
  try {
    const result = await models.Tag.create(request.payload)
    reply(result)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function read (request, reply) {
  try {
    const result = await models.Tag.findById(request.params.id)
    if (!result) {
      return reply.notFound()
    }
    reply(result)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}
