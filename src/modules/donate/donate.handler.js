'use strict'

const models = require('../../models')
const trace = require('../../services/trace')

module.exports = {
  list, create
}

async function create (request, reply) {
  try {
    const userId = request.auth.credentials.id
    const initiativeId = request.params.initiativeId

    const user = await models.User.findById(userId)

    const value = request.payload.value
    const newUserCoins = user.coins - value
    const userLevel = user.userLevel()

    if (newUserCoins < 0) {
      return reply.badData('insufficient funds')
    }

    if (userLevel < 4) {
      return reply.badData('insufficient level')
    }

    request.payload.initiativeId = initiativeId
    request.payload.userId = userId

    const result = await models.Donate.create(request.payload)

    await user.update({coins: newUserCoins})

    trace({
      type: 'actionDonate',
      userId: userId,
      initiativeId: initiativeId
    })

    reply(result)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function list (request, reply) {
  try {
    const options = {
      limit: request.query.limit,
      offset: request.query.skip,
      where: {
        initiativeId: request.params.initiativeId
      },
      distinct: true,
      include: [{
        model: models.User,
        as: 'user',
        attributes: ['id', 'firstName']
      }]
    }
    const result = await models.Donate.findAndCountAll(options)
    const response = {
      total: result.count,
      skip: request.query.skip,
      limit: request.query.limit,
      items: result.rows
    }
    reply(response)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}
