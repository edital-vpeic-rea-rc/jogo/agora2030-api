/* global describe, before, it, expect, server */

const utils = require('../../../test/utils')

let initiativeId = null
let ENDPOINT = null
let initiatives = null

describe(`#Donate`, () => {
  let userToken = null // admin is lvl 3 (xp 150)
  let adminToken = null // admin is lvl 2 (xp 100)
  before((done) => {
    utils.getToken(server)
    .then((userContext) => {
      userToken = userContext.token
      utils.getToken(server, 'admin')
      .then((adminContext) => {
        adminToken = adminContext.token
        utils.getInitiatives()
        .then(data => {
          initiatives = data
          initiativeId = data[0].id
          ENDPOINT = `/v1/initiatives/${initiativeId}/donates`
          done()
        })
      })
    })
  })

  describe(`POST`, () => {
    it('donate 50', (done) => {
      const options = {
        method: 'POST',
        url: ENDPOINT,
        headers: {
          'Authorization': `Bearer ${userToken}`
        },
        payload: {
          value: 50
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        expect(response.result).to.exist()
        expect(response.result.initiativeId).to.equals(initiativeId)
        expect(response.result.value).to.equals(50)
        utils.getUser()
        .then(user => {
          expect(user.coins).to.equals(50)
          done()
        })
      })
    })

    it('donate > 50', (done) => {
      const options = {
        method: 'POST',
        url: ENDPOINT,
        headers: {
          'Authorization': `Bearer ${userToken}`
        },
        payload: {
          value: 60
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(400)
        done()
      })
    })

    it('donate +50', (done) => {
      const options = {
        method: 'POST',
        url: `/v1/initiatives/${initiatives[1].id}/donates`,
        headers: {
          'Authorization': `Bearer ${userToken}`
        },
        payload: {
          value: 50
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        expect(response.result).to.exist()
        expect(response.result.initiativeId).to.equals(initiatives[1].id)
        expect(response.result.value).to.equals(50)
        utils.getUser()
        .then(user => {
          expect(user.coins).to.equals(0)
          done()
        })
      })
    })

    it('insufficient funds', (done) => {
      const options = {
        method: 'POST',
        url: ENDPOINT,
        headers: {
          'Authorization': `Bearer ${userToken}`
        },
        payload: {
          value: 20
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(422)
        expect(response.result).to.exist()
        expect(response.result.message).to.equals('insufficient funds')
        done()
      })
    })

    it('insufficient level', (done) => {
      const options = {
        method: 'POST',
        url: ENDPOINT,
        headers: {
          'Authorization': `Bearer ${adminToken}`
        },
        payload: {
          value: 20
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(422)
        expect(response.result).to.exist()
        expect(response.result.message).to.equals('insufficient level')
        done()
      })
    })

    it('not found initiativeId', (done) => {
      const options = {
        method: 'POST',
        url: `/v1/initiatives/09919733-3d2f-4d7a-a9bd-ad6d4c3822e7/donates`,
        headers: {
          'Authorization': `Bearer ${userToken}`
        },
        payload: {
          value: 20
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(422)
        done()
      })
    })
  })

  describe(`GET`, () => {
    it('get all', (done) => {
      const options = {
        method: 'GET',
        url: ENDPOINT,
        headers: {'Authorization': `Bearer ${userToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        expect(response.result).to.exist()
        expect(response.result.total).to.greaterThan(0)
        expect(response.result.skip).to.exist()
        expect(response.result.limit).to.exist()
        expect(response.result.items).to.not.be.empty()
        done()
      })
    })

    it('not found initiativeId', (done) => {
      const options = {
        method: 'GET',
        url: `/v1/initiatives/09919733-3d2f-4d7a-a9bd-ad6d4c3822e7/donates`,
        headers: {'Authorization': `Bearer ${userToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        expect(response.result).to.exist()
        expect(response.result.total).to.equals(0)
        expect(response.result.skip).to.exist()
        expect(response.result.limit).to.exist()
        expect(response.result.items).to.be.empty()
        done()
      })
    })
  })
})
