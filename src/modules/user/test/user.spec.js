/* global describe, before, it, expect, server */

const utils = require('../../../test/utils')
const models = require('../../../models')
const trace = require('../../../services/trace')

describe(`#User`, () => {
  let userToken = null
  let initiatives = null
  before((done) => {
    utils.getToken(server)
    .then((userContext) => {
      userToken = userContext.token
      utils.getInitiatives()
      .then(data => {
        initiatives = data
        models.Mission.create({
          title: 'Dev',
          description: 'O mundo precisa de devs',
          order: 7,
          actionLike: 2
        })
        done()
      })
    })
  })

  describe(`User Missions`, () => {
    it('complete one mission', (done) => {
      utils.getUser()
      .then(user => {
        trace({
          type: 'actionLike',
          userId: user.id,
          initiativeId: initiatives[0].id
        })
      .then(ok => {
        trace({
          type: 'actionLike',
          userId: user.id,
          initiativeId: initiatives[1].id
        })
        .then(ok => {
          models.User.scope('missions').findOne({where: {email: 'user@mail.com'}})
          .then(userMissions => {
            expect(userMissions.missions.length).to.equals(1)
            done()
          })
        })
      })
      })
    })
  })
})
