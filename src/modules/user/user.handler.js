'use strict'

const models = require('../../models')
const fs = require('fs-extra')
const config = require('../../../config/env')
const imageUploadService = require('../../services/fileUpload')
const Helpers = require('../../core/lib/helpers')
const Op = models.Sequelize.Op
const crypto = require('../../core/lib/crypto')
const handlebars = require('handlebars')
const templates = require('../../notifications/templates')
const htmlPdf = require('html-pdf')
const notification = require('../../notifications/mail')
const moment = require('moment')

module.exports = {
  list,
  read,
  update,
  profile,
  traceStats,
  activities,
  avatar,
  downloadCertificate,
  generateCertificate,
  curators
}

async function list (request, reply) {
  try {
    const options = {
      where: {},
      limit: request.query.limit,
      offset: request.query.skip,
      order: [['xp', 'DESC']]
    }

    if (request.query.level) {
      options.where.xp = {
        [Op.between]: Helpers.userXpRange(request.query.level)
      }
    }

    const scope = request.query.scope
    const result = await models.User.scope(scope).findAndCountAll(options)
    const response = {
      total: result.count,
      skip: request.query.skip,
      limit: request.query.limit,
      items: result.rows
    }
    reply(response)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function read (request, reply) {
  try {
    const scope = request.query.scope
    const result = await models.User.scope(scope).findById(request.params.id)
    if (!result) {
      reply.notFound()
    }
    reply(result)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function update (request, reply) {
  try {
    const { id } = request.auth.credentials
    const result = await models.User.update(request.payload, {where: {id}})
    if (result[0] === 0) {
      return reply.notFound()
    }
    const updated = await models.User.findById(id)
    let data = updated.toJSON()
    if (request.payload.receiveEmail) {
      data.receiveEmail = request.payload.receiveEmail
    }
    if (request.payload.receivePush) {
      data.receivePush = request.payload.receivePush
    }
    reply(data)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function profile (request, reply) {
  try {
    const { id } = request.auth.credentials
    const scope = request.query.scope
    const profile = await models.User.scope(scope).findById(id)
    reply(profile)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function traceStats (request, reply) {
  try {
    const { id } = request.auth.credentials
    const trace = await models.Trace.findAll({
      attributes: [
        'type',
        [models.sequelize.fn('COUNT', 'type'), 'numberOfActions']
      ],
      group: ['type'],
      where: {
        userId: id
      }
    })

    let result = {
      actionLike: 0,
      actionView: 0,
      actionShare: 0,
      actionDonate: 0,
      actionFavorite: 0,
      actionComment: 0
    }

    for (let i = 0; i < trace.length; i++) {
      let element = trace[i].toJSON()
      result[element.type] = element.numberOfActions
    }

    reply(result)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function activities (request, reply) {
  try {
    const userId = request.params.id || request.auth.credentials.id

    const options = {
      userId,
      limit: request.query.limit,
      offset: request.query.skip
    }

    if (request.query.type) {
      options.filterType = request.query.type
    }

    const result = await userActivities(options)

    reply(result)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function avatar (request, reply) {
  try {
    const userId = request.auth.credentials.id

    const opts = {
      model: 'User',
      id: userId
    }

    const avatarPath = config.uploadDir + imageUploadService.getFolderPath(opts, 'image', 'avatar')

    fs.removeSync(avatarPath)

    const image = await imageUploadService.upload(request.payload, opts)

    const data = {
      avatarPath: image.folderPath,
      avatarExtName: image.extName,
      socialAvatar: null
    }

    const result = await models.User.update(data, {where: {id: userId}})

    if (result[0] === 0) {
      return reply.notFound()
    }

    const updated = await models.User.scope('resume').findById(userId)

    reply(updated)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function curators (request, reply) {
  try {
    const missionCuradoria = await models.Mission.findOne({
      where: {title: 'Curadoria'},
      attributes: ['id']
    })

    const result = await models.User.findAndCountAll({
      attributes: [
        'firstName', 'lastName', 'avatarPath',
        'state', 'city', 'major', 'socialAvatar', 'avatarExtName', 'createdAt'
      ],
      subQuery: false,
      limit: 11,
      order: [['createdAt', 'DESC']],
      include: [
        {
          where: {id: missionCuradoria.id},
          association: 'missions',
          attributes: [],
          through: {attributes: []}
        }
      ]
    })
    const response = {
      total: result.count,
      items: result.rows.map(item => {
        let data = item.toJSON()
        delete data.nivel
        delete data.socialLoginOrigins
        delete data.createdAt
        return data
      })
    }

    reply(response)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function generateCertificate (request, reply) {
  try {
    const userId = request.auth.credentials.id

    const curadoriaMission = await models.Mission.findOne({
      attributes: ['id'],
      where: {title: 'Curadoria'}
    })

    if (!curadoriaMission) {
      return reply.notFound('Mission Curadoria not found.')
    }

    const userCuradoriaMission = await models.UserMissions.findOne({
      where: {
        missionId: curadoriaMission.id,
        userId: userId
      }
    })

    if (!userCuradoriaMission) {
      return reply.notFound('User não possui a missão Curadoria.')
    }

    const user = await models.User.findOne({
      attributes: ['firstName', 'lastName', 'email'],
      where: {id: userId}
    })

    if (!user) {
      // it's so baadd..
      return reply.notFound('User not found.')
    }

    moment.locale('pt-br')
    const dateFomated = moment(userCuradoriaMission.createdAt).format('LL')

    const hash = crypto.encrypt(JSON.stringify({
      dateFomated,
      name: `${user.firstName} ${user.lastName}`
    }))

    const urlCertificate = `${config.apiUrl}/user/certificate?data=${hash}`

    const optionsNot = {
      template: `MissaoCuradoria`,
      data: {email: user.email, urlCertificate}
    }

    if (config.env !== 'test') {
      notification.sendMail(optionsNot)
      .then(sucess => {})
      .catch((err) => { console.log('Error: send email mission: ', err) })
    }

    reply({message: 'Ok'})
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function downloadCertificate (request, reply) {
  try {
    if (!request.query.data) {
      return reply.badData('Não possui dado para ser gerado')
    }

    let data = JSON.parse(crypto.decrypt(request.query.data))
    const template = handlebars.compile(templates['Certificate'])
    const html = template(data)

    const options = {
      format: 'A4',
      orientation: 'landscape'
    }

    htmlPdf.create(html, options).toStream((err, pdfStream) => {
      if (err) {
        console.log(err)
        return reply.badData('Erro ao gerar certificado! =|')
      } else {
        reply(pdfStream)
      }
    })
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function userActivities (options) {
  try {
    let whereOpt = {userId: options.userId}

    if (options.filterType) {
      whereOpt.type = options.filterType
      const result = await models.Trace.findAll({
        attributes: ['type', 'createdAt'],
        order: [['createdAt', 'DESC']],
        where: whereOpt,
        distinct: true,
        subQuery: false,
        include: [
          {
            model: models.Initiative,
            as: 'initiative',
            attributes: ['id', 'title'],
            include: [
              {
                association: 'coverImage'
              },
              {
                model: models.Ods,
                as: 'ods',
                attributes: ['id', 'number', 'color'],
                through: {attributes: []}
              }
            ]
          }
        ]
      })

      return result
    }

    const result = await models.Trace.findAndCountAll({
      limit: options.limit,
      offset: options.skip,
      attributes: ['type', 'createdAt'],
      order: [['createdAt', 'DESC']],
      where: whereOpt,
      include: [
        {
          model: models.Initiative,
          as: 'initiative',
          attributes: ['id', 'title']
        }
      ]
    })

    const response = {
      total: result.count,
      skip: options.skip,
      limit: options.limit,
      items: result.rows
    }

    return response
  } catch (error) {
    console.log('Erro ao pegar atividades: ', options, error)
  }
}
