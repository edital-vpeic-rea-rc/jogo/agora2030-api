'use strict'

const Handler = require('./user.handler')
const Schema = require('./user.schema')
const Helpers = require('../../core/lib/helpers')
const config = require('../../../config/env')

module.exports = [
  {
    method: 'GET',
    path: '/users',
    config: {
      description: 'GET users',
      notes: 'Get all users',
      tags: ['api', 'user'],
      auth: {
        scope: ['admin', 'user']
      },
      handler: Handler.list,
      validate: {
        query: Schema.query
      },
      response: {
        schema: Helpers.Joi.validate.pagination.response
      }
    }
  },
  {
    method: 'GET',
    path: '/users/{id}',
    config: {
      description: 'GET one user',
      notes: 'Get one user by id',
      tags: ['api', 'user'],
      auth: {
        scope: ['admin', 'user']
      },
      handler: Handler.read,
      validate: {
        query: Schema.scope,
        params: {id: Helpers.Joi.validate.uuidv4}
      }
    }
  },
  {
    method: 'GET',
    path: '/users/{id}/activities',
    config: {
      description: 'Get user activities',
      tags: ['api', 'profile', 'activities'],
      auth: {
        scope: ['admin', 'user']
      },
      validate: {
        query: Schema.query,
        params: {id: Helpers.Joi.validate.uuidv4}
      },
      handler: Handler.activities
    }
  },
  {
    method: 'GET',
    path: '/curators',
    config: {
      description: 'GET all curators',
      tags: ['api', 'user'],
      auth: false,
      handler: Handler.curators
    }
  },
  {
    method: 'GET',
    path: '/profile',
    config: {
      description: 'Get user profile',
      tags: ['api', 'profile'],
      auth: {
        scope: ['admin', 'user']
      },
      handler: Handler.profile,
      validate: {
        query: Schema.scopeProfile
      }
    }
  },
  {
    method: 'GET',
    path: '/profile/tracestats',
    config: {
      description: 'Get user stats',
      tags: ['api', 'profile', 'stats'],
      auth: {
        scope: ['admin', 'user']
      },
      handler: Handler.traceStats
    }
  },
  {
    method: 'GET',
    path: '/profile/activities',
    config: {
      description: 'Get user activities',
      tags: ['api', 'profile', 'activities'],
      auth: {
        scope: ['admin', 'user']
      },
      validate: {
        query: Schema.query
      },
      handler: Handler.activities
    }
  },
  {
    method: 'GET',
    path: '/user/certificate',
    config: {
      description: 'Get user mission certificate',
      tags: ['api', 'profile', 'certificate'],
      auth: false,
      handler: Handler.downloadCertificate
    }
  },
  {
    method: 'POST',
    path: '/user/certificate',
    config: {
      description: 'Generate user certificate',
      tags: ['api', 'profile', 'certificate'],
      auth: {
        scope: ['admin', 'user']
      },
      handler: Handler.generateCertificate
    }
  },
  {
    method: 'PATCH',
    path: '/profile',
    config: {
      description: 'Update user profile',
      tags: ['api', 'profile'],
      auth: {
        scope: ['admin', 'user']
      },
      handler: Handler.update,
      validate: {
        payload: Schema.update
      }
    }
  },
  {
    method: 'POST',
    path: '/profile/setavatar',
    config: {
      description: 'Upload user avatar',
      notes: 'If avatar exists, will be removed and created a new',
      tags: ['api', 'user'],
      payload: {
        output: 'stream',
        parse: true,
        allow: 'multipart/form-data',
        maxBytes: config.fileSize
      },
      auth: {
        scope: ['admin', 'user']
      },
      handler: Handler.avatar
    }
  }
]
