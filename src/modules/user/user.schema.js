'use strict'

const Joi = require('joi')
const Helpers = require('../../core/lib/helpers')
const countries = require('../../../data/util/countries')
const majors = require('../../../data/util/majors')

const schema = {
  firstName: Joi.string(),
  lastName: Joi.string(),
  email: Joi.string().email(),
  password: Joi.string().min(6),
  country: Joi.only(countries),
  state: Joi.string().max(30).allow(''),
  city: Joi.string().max(40).allow(''),
  role: Joi.string(),
  xp: Joi.number().integer(),
  coins: Joi.number().integer(),
  gender: Joi.only(['Masculino', 'Feminino']).allow(''),
  major: Joi.only(majors).allow(''),
  occupation: Joi.string().max(40).allow(''),
  phoneNumber: Joi.string().max(20).allow(''),
  receiveEmail: Joi.boolean(),
  receivePush: Joi.boolean()
}

const update = Joi.object({
  firstName: schema.firstName.optional(),
  lastName: schema.lastName.optional(),
  email: schema.email.optional(),
  password: schema.password.optional(),
  country: schema.country.optional(),
  state: schema.state.optional(),
  city: schema.city.optional(),
  role: schema.role.forbidden(),
  xp: schema.xp.forbidden(),
  coins: schema.coins.forbidden(),
  gender: schema.gender.optional(),
  major: schema.major.optional(),
  occupation: schema.occupation.optional(),
  phoneNumber: schema.phoneNumber.optional(),
  receiveEmail: schema.receiveEmail.optional(),
  receivePush: schema.receivePush.optional()
}).required().min(1)

const scope = Joi.object({
  scope: Joi.only(['public', 'resume', 'missions', 'list']).default('list')
})

const scopeProfile = Joi.object({
  scope: Joi.only(['public', 'resume', 'me']).default('me')
})

const query = Helpers.Joi.validate.pagination.query.concat(Joi.object({
  scope: Joi.only(['public', 'resume', 'missions', 'list']).default('list'),
  level: Joi.number(),
  type: Joi.only(['actionLike', 'actionView', 'actionShare', 'actionDonate', 'actionFavorite', 'actionComment'])
}))

module.exports = {
  update, query, scope, scopeProfile
}
