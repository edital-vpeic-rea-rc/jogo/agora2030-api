/* global describe, before, it, expect, server */

const utils = require('../../../test/utils')

let initiativeId = null
let ENDPOINT = null

describe(`#Share`, () => {
  let userToken = null

  before((done) => {
    utils.getToken(server)
    .then((userContext) => {
      userToken = userContext.token
      utils.getInitiative()
      .then(initiative => {
        initiativeId = initiative
        ENDPOINT = `/v1/initiatives/${initiativeId}/share`
        done()
      })
    })
  })

  describe(`POST`, () => {
    it('create a new', (done) => {
      const options = {
        method: 'POST',
        url: ENDPOINT,
        headers: {
          'Authorization': `Bearer ${userToken}`
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        done()
      })
    })
    it('not found initiativeId', (done) => {
      const options = {
        method: 'POST',
        url: `/v1/initiatives/09919733-3d2f-4d7a-a9bd-ad6d4c3822e7/share`,
        headers: {
          'Authorization': `Bearer ${userToken}`
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(404)
        done()
      })
    })
  })
})
