/* global describe, before, it, expect, server */

const utils = require('../../../test/utils')

let initiativeId = null
let ENDPOINT = null

describe(`#Comment`, () => {
  let adminToken = null
  let userToken = null
  let idCommentAdmin = null
  let idCommentUser = null

  before((done) => {
    utils.getToken(server)
    .then((userContext) => {
      userToken = userContext.token
      utils.getToken(server, 'admin')
      .then((adminContext) => {
        adminToken = adminContext.token
        utils.getInitiative()
        .then(data => {
          initiativeId = data
          ENDPOINT = `/v1/initiatives/${initiativeId}/comments`
          done()
        })
      })
    })
  })

  describe(`POST`, () => {
    it('create a new (admin)', (done) => {
      const options = {
        method: 'POST',
        url: ENDPOINT,
        headers: {
          'Authorization': `Bearer ${adminToken}`
        },
        payload: {
          text: 'Comment 1'
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        expect(response.result).to.exist()
        expect(response.result.text).to.exists()
        expect(response.result.user.firstName).to.exists()
        idCommentAdmin = response.result.id
        done()
      })
    })

    it('create a new (user)', (done) => {
      const options = {
        method: 'POST',
        url: ENDPOINT,
        headers: {
          'Authorization': `Bearer ${userToken}`
        },
        payload: {
          text: 'Comment 2'
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        expect(response.result).to.exist()
        expect(response.result.text).to.exists()
        idCommentUser = response.result.id
        done()
      })
    })

    it('not send required data', (done) => {
      const options = {
        method: 'POST',
        url: ENDPOINT,
        headers: {
          'Authorization': `Bearer ${adminToken}`
        },
        payload: {
          text: ''
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(400)
        done()
      })
    })
  })

  describe(`GET`, () => {
    it('get all', (done) => {
      const options = {
        method: 'GET',
        url: ENDPOINT,
        headers: {'Authorization': `Bearer ${adminToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        expect(response.result).to.exist()
        expect(response.result.total).to.greaterThan(0)
        expect(response.result.skip).to.exist()
        expect(response.result.limit).to.exist()
        expect(response.result.items).to.not.be.empty()
        done()
      })
    })
    it('not found', (done) => {
      const options = {
        method: 'GET',
        url: `/v1/initiatives/09919733-3d2f-4d7a-a9bd-ad6d4c3822e7/comments`,
        headers: {'Authorization': `Bearer ${adminToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        expect(response.result).to.exist()
        expect(response.result.total).to.equals(0)
        expect(response.result.skip).to.exist()
        expect(response.result.limit).to.exist()
        expect(response.result.items).to.be.empty()
        done()
      })
    })
  })

  describe(`PATCH /{id}`, () => {
    it('update comment', (done) => {
      const options = {
        method: 'PATCH',
        url: `${ENDPOINT}/${idCommentAdmin}`,
        headers: {'Authorization': `Bearer ${adminToken}`},
        payload: {
          text: 'comment updated'
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        expect(response.result).to.exist()
        expect(response.result.id).to.equals(idCommentAdmin)
        expect(response.result.text).to.equals('comment updated')
        done()
      })
    })

    it('is not author (user)', (done) => {
      const options = {
        method: 'PATCH',
        url: `${ENDPOINT}/${idCommentAdmin}`,
        headers: {'Authorization': `Bearer ${userToken}`},
        payload: {
          text: 'comment updated'
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(422)
        done()
      })
    })

    it('is not author (admin)', (done) => {
      const options = {
        method: 'PATCH',
        url: `${ENDPOINT}/${idCommentUser}`,
        headers: {'Authorization': `Bearer ${adminToken}`},
        payload: {
          text: 'comment updated'
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(422)
        done()
      })
    })

    it('not found', (done) => {
      const options = {
        method: 'PATCH',
        url: `${ENDPOINT}/09919733-3d2f-4d7a-a9bd-ad6d4c3822e7`,
        headers: {'Authorization': `Bearer ${adminToken}`},
        payload: {
          text: 'text updated'
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(404)
        done()
      })
    })
  })

  describe(`DELETE /{id}`, () => {
    it('try delete comment (user -> admin)', (done) => {
      const options = {
        method: 'DELETE',
        url: `${ENDPOINT}/${idCommentAdmin}`,
        headers: {'Authorization': `Bearer ${userToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(422)
        done()
      })
    })

    it('try delete comment (user)', (done) => {
      const options = {
        method: 'DELETE',
        url: `${ENDPOINT}/${idCommentUser}`,
        headers: {'Authorization': `Bearer ${userToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        done()
      })
    })

    it('not found', (done) => {
      const options = {
        method: 'DELETE',
        url: `${ENDPOINT}/09919733-3d2f-4d7a-a9bd-ad6d4c3822e7`,
        headers: {'Authorization': `Bearer ${adminToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(404)
        done()
      })
    })

    it('invalid id', (done) => {
      const options = {
        method: 'DELETE',
        url: `${ENDPOINT}/invalid`,
        headers: {'Authorization': `Bearer ${adminToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(400)
        done()
      })
    })
  })
})
