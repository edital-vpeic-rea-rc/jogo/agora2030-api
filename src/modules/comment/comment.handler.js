'use strict'

const models = require('../../models')
const trace = require('../../services/trace')

module.exports = {
  list, create, update, destroy
}

async function list (request, reply) {
  try {
    const options = {
      limit: request.query.limit,
      offset: request.query.skip,
      where: {
        initiativeId: request.params.initiativeId
      },
      distinct: true,
      order: [['updatedAt', 'ASC']],
      include: [{
        model: models.User,
        as: 'user',
        required: true,
        attributes: [
          'id', 'firstName', 'lastName', 'avatarPath',
          'socialAvatar', 'avatarExtName']
      }]
    }
    const result = await models.Comment.findAndCountAll(options)
    const response = {
      total: result.count,
      skip: request.query.skip,
      limit: request.query.limit,
      items: result.rows
    }
    reply(response)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function create (request, reply) {
  try {
    const userId = request.auth.credentials.id
    const initiativeId = request.params.initiativeId

    request.payload.initiativeId = initiativeId
    request.payload.userId = userId

    const created = await models.Comment.create(request.payload)

    trace({
      type: 'actionComment',
      userId: userId,
      initiativeId: initiativeId
    })

    const result = await models.Comment.findOne({
      where: {
        id: created.id
      },
      include: [{
        model: models.User,
        as: 'user',
        attributes: [
          'id', 'firstName', 'lastName', 'avatarPath',
          'socialAvatar', 'avatarExtName']
      }]
    })

    reply(result)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function update (request, reply) {
  try {
    const comment = await models.Comment.findOne({
      where: {
        initiativeId: request.params.initiativeId,
        id: request.params.id
      },
      include: [{model: models.User, as: 'user', attributes: ['id']}]
    })

    if (!comment) {
      return reply.notFound()
    }

    const hasPermission = allowModify(request.auth.credentials, comment.toJSON())

    if (hasPermission.allowed === false || hasPermission.methods.indexOf('update') === -1) {
      return reply.badData('You are not authorized')
    }

    await comment.update(request.payload)

    const updated = await models.Comment.findOne({
      where: {
        id: request.params.id
      },
      include: [{
        model: models.User,
        as: 'user',
        attributes: [
          'id', 'firstName', 'lastName', 'avatarPath',
          'socialAvatar', 'avatarExtName']
      }]
    })

    reply(updated)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function destroy (request, reply) {
  try {
    const comment = await models.Comment.findOne({
      where: {
        initiativeId: request.params.initiativeId,
        id: request.params.id
      },
      include: [{model: models.User, as: 'user', attributes: ['id']}]
    })

    if (!comment) {
      return reply.notFound()
    }

    const hasPermission = allowModify(request.auth.credentials, comment.toJSON())

    if (hasPermission.allowed === false || hasPermission.methods.indexOf('destroy') === -1) {
      return reply.badData('You are not authorized')
    }

    await comment.destroy()

    reply({message: 'Ok'})
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

function allowModify (auth, comment) {
  let permission = {
    allowed: false,
    methods: []
  }

  if (auth.scope[0] === 'admin') {
    permission.allowed = true
    permission.methods = ['destroy']
  }

  if (comment.user.id === auth.id) {
    permission.allowed = true
    permission.methods = ['destroy', 'update']
  }

  return permission
}
