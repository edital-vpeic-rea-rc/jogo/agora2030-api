'use strict'

const Joi = require('joi')

const schema = {
  text: Joi.string().max(500)
}

const create = Joi.object({
  text: schema.text.required()
})

const update = Joi.object({
  text: schema.text.required()
})

module.exports = {
  create, update
}
