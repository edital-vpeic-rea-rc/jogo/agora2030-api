'use strict'

const models = require('../../models')

module.exports = {
  list, create, destroy
}

async function list (request, reply) {
  try {
    const options = {
      limit: request.query.limit,
      offset: request.query.skip,
      where: {
        initiativeId: request.params.initiativeId
      }
    }
    if (request.query.type) {
      options.where.type = request.query.type
    }
    const result = await models.Url.findAndCountAll(options)
    const response = {
      total: result.count,
      skip: request.query.skip,
      limit: request.query.limit,
      items: result.rows
    }
    reply(response)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function create (request, reply) {
  try {
    if (!validateUrl(request.payload)) {
      return reply.badData(`${request.payload.url} is a invalid url to ${request.payload.type} type`)
    }
    request.payload.initiativeId = request.params.initiativeId
    const result = await models.Url.create(request.payload)
    reply(result)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function destroy (request, reply) {
  try {
    const options = {
      where: {
        id: request.params.id,
        initiativeId: request.params.initiativeId
      }
    }
    const result = await models.Url.destroy(options)
    if (!result) {
      return reply.notFound()
    }
    reply({message: 'OK'})
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

function validateUrl (payload) {
  const {url, type} = payload
  const domain = getUrlDomain(url)
  const validMediaDomains = ['youtube.com', 'youtu.be', 'vimeo.com', 'soundcloud.com']

  if (type === 'video' || type === 'audio') {
    if (validMediaDomains.indexOf(domain) === -1) {
      return false
    }
  }
  return true
}

function getUrlDomain (url) {
  let domain
  // find & remove protocol (http, ftp, etc.) and get domain
  if (url.indexOf('://') > -1) {
    domain = url.split('/')[2]
  } else {
    domain = url.split('/')[0]
  }

  domain = domain.split(':')[0] // remove port

  if (domain.indexOf('www.') > -1) {
    domain = domain.split('www.')[1] // remove www
  }

  return domain
}
