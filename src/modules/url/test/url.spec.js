/* global describe, before, it, expect, server */

const utils = require('../../../test/utils')

let initiativeId = null
let ENDPOINT = null
let id = null

describe(`#Url`, () => {
  let adminToken = null
  before((done) => {
    utils.getToken(server, 'admin')
    .then((adminContext) => {
      adminToken = adminContext.token
      utils.getInitiative()
      .then(initiative => {
        initiativeId = initiative
        ENDPOINT = `/v1/initiatives/${initiativeId}/urls`
        done()
      })
    })
  })

  describe(`POST`, () => {
    it('create a new', (done) => {
      const options = {
        method: 'POST',
        url: ENDPOINT,
        headers: {
          'Authorization': `Bearer ${adminToken}`
        },
        payload: {
          url: 'https://www.youtube.com/watch?v=4FYPMtm--Rk',
          type: 'video',
          title: 'Video muito loco'
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        expect(response.result).to.exist()
        expect(response.result.url).to.exists()
        expect(response.result.title).to.exists()
        id = response.result.id
        done()
      })
    })

    it('invalid url', (done) => {
      const options = {
        method: 'POST',
        url: ENDPOINT,
        headers: {
          'Authorization': `Bearer ${adminToken}`
        },
        payload: {
          url: 'invalid',
          type: 'video'
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(400)
        done()
      })
    })

    it('invalid url to type', (done) => {
      const options = {
        method: 'POST',
        url: ENDPOINT,
        headers: {
          'Authorization': `Bearer ${adminToken}`
        },
        payload: {
          url: 'https://www.invalid.com/watch?v=4FYPMtm--Rk',
          type: 'video'
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(422)
        done()
      })
    })
  })

  describe(`GET`, () => {
    it('get all', (done) => {
      const options = {
        method: 'GET',
        url: ENDPOINT,
        headers: {'Authorization': `Bearer ${adminToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        expect(response.result).to.exist()
        expect(response.result.total).to.greaterThan(0)
        expect(response.result.skip).to.exist()
        expect(response.result.limit).to.exist()
        expect(response.result.items).to.not.be.empty()
        done()
      })
    })
    it('filter by type (video)', (done) => {
      const options = {
        method: 'GET',
        url: `${ENDPOINT}?type=video`,
        headers: {'Authorization': `Bearer ${adminToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        expect(response.result).to.exist()
        expect(response.result.total).to.greaterThan(0)
        expect(response.result.skip).to.exist()
        expect(response.result.limit).to.exist()
        expect(response.result.items).to.not.be.empty()
        done()
      })
    })
    it('filter by type (audio)', (done) => {
      const options = {
        method: 'GET',
        url: `${ENDPOINT}?type=audio`,
        headers: {'Authorization': `Bearer ${adminToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        expect(response.result).to.exist()
        expect(response.result.total).to.equals(0)
        expect(response.result.skip).to.exist()
        expect(response.result.limit).to.exist()
        expect(response.result.items).to.be.empty()
        done()
      })
    })
  })
  describe(`DELETE /{id}`, () => {
    it('delete one', (done) => {
      const options = {
        method: 'DELETE',
        url: `${ENDPOINT}/${id}`,
        headers: {'Authorization': `Bearer ${adminToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        done()
      })
    })
  })
})
