'use strict'

const Joi = require('joi')
const Helpers = require('../../core/lib/helpers')

const schema = {
  url: Joi.string().uri(),
  title: Joi.string().max(50).allow(''),
  type: Joi.only(['audio', 'video', 'link'])
}

const create = Joi.object({
  url: schema.url.required(),
  title: schema.title.optional(),
  type: schema.type.required()
})

const query = Helpers.Joi.validate.pagination.query.concat(
  Joi.object({
    type: Joi.only(['audio', 'video', 'link'])
  })
)

module.exports = {
  create, query
}
