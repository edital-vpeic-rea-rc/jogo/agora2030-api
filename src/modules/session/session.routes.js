'use strict'

const Handler = require('./session.handler')
const Schema = require('./session.schema')

module.exports = [
  {
    method: 'POST',
    path: '/signup',
    config: {
      description: 'POST signup',
      notes: 'Create a new user',
      tags: ['api', 'user', 'session'],
      auth: false,
      handler: Handler.signup,
      validate: {
        payload: Schema.signup
      }
    }
  },
  {
    method: 'POST',
    path: '/login',
    config: {
      description: 'POST login',
      notes: 'Authenticate user',
      tags: ['api', 'user', 'session'],
      auth: false,
      handler: Handler.login,
      validate: {
        payload: Schema.login
      }
    }
  },
  {
    method: 'POST',
    path: '/socialsignup',
    config: {
      description: 'Signup user from social media',
      tags: ['api', 'user', 'session'],
      auth: false,
      handler: Handler.socialSignup,
      validate: {
        payload: Schema.socialSignup
      }
    }
  },
  {
    method: 'POST',
    path: '/sociallogin',
    config: {
      description: 'Authenticate user from social media',
      tags: ['api', 'user', 'session'],
      auth: false,
      handler: Handler.socialLogin,
      validate: {
        payload: Schema.socialLogin
      }
    }
  },
  {
    method: 'GET',
    path: '/confirmation/{token}',
    config: {
      description: 'Confirm signup',
      tags: ['api', 'user', 'session'],
      auth: false,
      handler: Handler.confirmationSignup
    }
  },
  {
    method: 'POST',
    path: '/recovery/password',
    config: {
      description: 'Recovery password',
      tags: ['api', 'user', 'session'],
      auth: false,
      handler: Handler.solRecoveryPassword,
      validate: {
        payload: Schema.solRecoveryPassword
      }
    }
  },
  {
    method: 'POST',
    path: '/recovery/password/{token}',
    config: {
      description: 'Recovery password',
      tags: ['api', 'user', 'session'],
      auth: false,
      handler: Handler.recoveryPassword,
      validate: {
        payload: Schema.recoveryPassword
      }
    }
  }
]
