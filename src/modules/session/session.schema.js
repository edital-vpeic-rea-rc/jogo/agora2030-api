'use strict'

const Joi = require('joi')
const countries = require('../../../data/util/countries')
const majors = require('../../../data/util/majors')

const schema = {
  firstName: Joi.string(),
  lastName: Joi.string(),
  email: Joi.string().email(),
  password: Joi.string().min(6),
  socialLoginOrigin: Joi.only(['facebook', 'google']),
  socialAvatar: Joi.string().allow(''),
  country: Joi.only(countries),
  state: Joi.string().max(30).allow(''),
  city: Joi.string().max(40).allow(''),
  role: Joi.string(),
  xp: Joi.number().integer(),
  coins: Joi.number().integer(),
  gender: Joi.only(['Masculino', 'Feminino']).allow(''),
  major: Joi.only(majors).allow(''),
  occupation: Joi.string().max(40).allow(''),
  phoneNumber: Joi.string().max(20).allow(''),
  receiveEmail: Joi.boolean(),
  receivePush: Joi.boolean()
}

const signup = Joi.object({
  firstName: schema.firstName.required(),
  lastName: schema.lastName.required(),
  email: schema.email.required(),
  password: schema.password.required(),
  country: schema.country.optional(),
  state: schema.state.optional(),
  city: schema.city.optional(),
  role: schema.role.forbidden(),
  xp: schema.xp.forbidden(),
  coins: schema.coins.forbidden(),
  gender: schema.gender.optional(),
  major: schema.major.optional(),
  occupation: schema.occupation.optional(),
  phoneNumber: schema.phoneNumber.optional(),
  receiveEmail: schema.receiveEmail.optional(),
  receivePush: schema.receivePush.optional()
})

const socialSignup = Joi.object({
  firstName: schema.firstName.required(),
  lastName: schema.lastName.required(),
  email: schema.email.required(),
  socialLoginOrigin: schema.socialLoginOrigin.required(),
  socialAvatar: schema.socialAvatar.optional(),
  country: schema.country.optional(),
  state: schema.state.optional(),
  city: schema.city.optional(),
  role: schema.role.forbidden(),
  xp: schema.xp.forbidden(),
  coins: schema.coins.forbidden(),
  gender: schema.gender.optional(),
  major: schema.major.optional(),
  occupation: schema.occupation.optional(),
  phoneNumber: schema.phoneNumber.optional(),
  receiveEmail: schema.receiveEmail.optional(),
  receivePush: schema.receivePush.optional()
})

const login = Joi.object({
  email: schema.email.required(),
  password: schema.password.required()
})

const socialLogin = Joi.object({
  email: schema.email.required(),
  socialLoginOrigin: schema.socialLoginOrigin.required(),
  socialAvatar: schema.socialAvatar.optional(),
  accessToken: Joi.string().required()
})

const solRecoveryPassword = Joi.object({
  email: schema.email.required()
})

const recoveryPassword = Joi.object({
  password: schema.password.required()
})

module.exports = {
  signup, socialSignup, login, socialLogin, solRecoveryPassword, recoveryPassword
}
