/* global describe, it, expect, server */

const models = require('../../../models')

const ENDPOINT_SIGNUP = '/v1/signup'
const ENDPOINT_LOGIN = '/v1/login'
const ENDPOINT_SOCIAL_LOGIN = '/v1/socialsignup'
const ENDPOINT_CONFIRM_SIGNUP = '/v1/confirmation'
const ENDPOINT_RECOVERY_PASSWORD = '/v1/recovery/password'

describe(`#Session`, () => {
  describe(`POST ${ENDPOINT_SIGNUP}`, () => {
    it('create a new user by normal signup', (done) => {
      const options = {
        method: 'POST',
        url: ENDPOINT_SIGNUP,
        payload: {
          firstName: 'David',
          lastName: 'Veloso',
          email: 'davidedsoon@gmail.com',
          password: '123456',
          country: 'Brasil'
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        expect(response.result).to.exist()
        expect(response.result.id).to.exist()
        done()
      })
    })

    it('valid unique email', (done) => {
      const options = {
        method: 'POST',
        url: ENDPOINT_SIGNUP,
        payload: {
          firstName: 'David',
          lastName: 'Veloso',
          email: 'davidedsoon@gmail.com',
          password: '123456',
          country: 'Brasil'
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(422)
        done()
      })
    })

    it('not send required data', (done) => {
      const options = {
        method: 'POST',
        url: ENDPOINT_SIGNUP,
        payload: {
          firstName: 'David'
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(400)
        done()
      })
    })
  })

  describe(`GET ${ENDPOINT_CONFIRM_SIGNUP}`, () => {
    it('email is not confirmed', (done) => {
      const options = {
        method: 'POST',
        url: ENDPOINT_LOGIN,
        payload: {
          email: 'davidedsoon@gmail.com',
          password: '123456'
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(422)
        done()
      })
    })
    it('token not found', (done) => {
      const options = {
        method: 'GET',
        url: `${ENDPOINT_CONFIRM_SIGNUP}/notexistenttoken`
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(404)
        models.User.findOne({where: {email: 'davidedsoon@gmail.com'}})
        .then(user => {
          expect(user.emailConfirmationToken).to.exist()
          done()
        })
      })
    })
    it('confirm email', (done) => {
      models.User.findOne({where: {email: 'davidedsoon@gmail.com'}})
      .then(user => {
        const options = {
          method: 'GET',
          url: `${ENDPOINT_CONFIRM_SIGNUP}/${user.emailConfirmationToken}`
        }
        server.inject(options, (response) => {
          expect(response.result).to.exist()
          expect(response.statusCode).to.equals(200)
          done()
        })
      })
    })
  })

  describe(`POST ${ENDPOINT_LOGIN}`, () => {
    it('make login and return token', (done) => {
      const options = {
        method: 'POST',
        url: ENDPOINT_LOGIN,
        payload: {
          email: 'davidedsoon@gmail.com',
          password: '123456'
        }
      }
      server.inject(options, (response) => {
        expect(response.result).to.exist()
        expect(response.result.token).to.exist()
        expect(response.result.firstName).to.exist()
        expect(response.result.password).to.not.exist()
        expect(response.statusCode).to.equals(200)
        done()
      })
    })

    it('not exists email', (done) => {
      const options = {
        method: 'POST',
        url: ENDPOINT_LOGIN,
        payload: {
          email: 'notexists@mail.com',
          password: 'testnotexists'
        }
      }
      server.inject(options, (response) => {
        expect(response.result.statusCode).to.equals(401)
        expect(response.result.error).to.equals('Unauthorized')
        done()
      })
    })

    it('incorrect password', (done) => {
      const options = {
        method: 'POST',
        url: ENDPOINT_LOGIN,
        payload: {
          email: 'davidedsoon@gmail.com',
          password: 'incorrect'
        }
      }
      server.inject(options, (response) => {
        expect(response.result.statusCode).to.equals(401)
        expect(response.result.error).to.equals('Unauthorized')
        done()
      })
    })
  })

  describe(`POST ${ENDPOINT_RECOVERY_PASSWORD}`, () => {
    it('sol recovery password', (done) => {
      const options = {
        method: 'POST',
        url: ENDPOINT_RECOVERY_PASSWORD,
        payload: {
          email: 'davidedsoon@gmail.com'
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        done()
      })
    })
    it('email not found', (done) => {
      const options = {
        method: 'POST',
        url: ENDPOINT_RECOVERY_PASSWORD,
        payload: {
          email: 'notfound@gmail.com'
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(404)
        done()
      })
    })
    it('token not found', (done) => {
      const options = {
        method: 'POST',
        url: `${ENDPOINT_RECOVERY_PASSWORD}/notexistenttoken`,
        payload: {
          password: 'newpasswordseted'
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(404)
        done()
      })
    })
    it('recovery password', (done) => {
      models.User.findOne({where: {email: 'davidedsoon@gmail.com'}})
      .then(user => {
        const options = {
          method: 'POST',
          url: `${ENDPOINT_RECOVERY_PASSWORD}/${user.recoveryPasswordToken}`,
          payload: {
            password: 'newpasswordseted'
          }
        }
        server.inject(options, (response) => {
          expect(response.result).to.exist()
          expect(response.statusCode).to.equals(200)
          done()
        })
      })
    })
    it('try make login with old password', (done) => {
      const options = {
        method: 'POST',
        url: ENDPOINT_LOGIN,
        payload: {
          email: 'davidedsoon@gmail.com',
          password: '123456'
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(401)
        done()
      })
    })
    it('try make login with new password', (done) => {
      const options = {
        method: 'POST',
        url: ENDPOINT_LOGIN,
        payload: {
          email: 'davidedsoon@gmail.com',
          password: 'newpasswordseted'
        }
      }
      server.inject(options, (response) => {
        expect(response.result).to.exist()
        expect(response.result.token).to.exist()
        expect(response.statusCode).to.equals(200)
        done()
      })
    })
  })

  describe(`POST ${ENDPOINT_SOCIAL_LOGIN}`, () => {
    it('create a new user by social login', (done) => {
      const options = {
        method: 'POST',
        url: ENDPOINT_SOCIAL_LOGIN,
        payload: {
          firstName: 'David',
          lastName: 'Veloso',
          email: 'davidedsoon@hotmail.com',
          country: 'Brasil',
          socialLoginOrigin: 'facebook',
          socialAvatar: 'https://scontent.fcpv3-1.fna.fbcdn.net/v/t1.0-9/484330_368889173200693_792645838_n.jpg?oh=f00e7b19e4d82766447aca9b5981c006&oe=5A86F5D4'
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        expect(response.result).to.exist()
        expect(response.result.token).to.exist()
        done()
      })
    })
    it('exists email', (done) => {
      const options = {
        method: 'POST',
        url: ENDPOINT_SOCIAL_LOGIN,
        payload: {
          firstName: 'David',
          lastName: 'Veloso',
          email: 'davidedsoon@hotmail.com',
          country: 'Brasil',
          socialLoginOrigin: 'facebook',
          socialAvatar: 'https://scontent.fcpv3-1.fna.fbcdn.net/v/t1.0-9/484330_368889173200693_792645838_n.jpg?oh=f00e7b19e4d82766447aca9b5981c006&oe=5A86F5D4'
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(422)
        expect(response.result.token).to.not.exist()
        done()
      })
    })
    it('not send required', (done) => {
      const options = {
        method: 'POST',
        url: ENDPOINT_SOCIAL_LOGIN,
        payload: {
          firstName: 'David',
          lastName: 'Veloso',
          email: 'davidedsoon@mail.com',
          country: 'Brasil'
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(400)
        done()
      })
    })
  })
})
