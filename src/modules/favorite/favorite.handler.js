'use strict'

const models = require('../../models')
const trace = require('../../services/trace')
const config = require('../../../config/env')

module.exports = {
  list, create
}

async function create (request, reply) {
  try {
    const userId = request.auth.credentials.id

    request.payload.userId = userId

    const user = await models.User.findById(userId)

    if (user.favorites === 0) {
      return reply.badData('Não possui mais favoritos')
    }

    const result = await models.Favorite.create(request.payload)

    trace({
      type: 'actionFavorite',
      userId: userId,
      initiativeId: request.payload.initiativeId
    })

    const favorites = user.favorites - 1
    await user.update({favorites})

    reply(result)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function list (request, reply) {
  try {
    const options = {
      limit: request.query.limit,
      offset: request.query.skip,
      where: {
        userId: request.auth.credentials.id
      },
      distinct: true,
      include: [{
        model: models.Initiative,
        as: 'initiative',
        attributes: ['id', 'title']
      }]
    }
    const result = await models.Favorite.findAndCountAll(options)
    const response = {
      total: result.count,
      skip: request.query.skip,
      limit: request.query.limit,
      items: result.rows
    }
    reply(response)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}
