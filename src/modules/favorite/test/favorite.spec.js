/* global describe, before, it, expect, server */

const utils = require('../../../test/utils')

let initiativeId = null
let ENDPOINT = `/v1/profile/favorites`
let initiatives = null

describe(`#Favorite`, () => {
  let userToken = null
  before((done) => {
    utils.getToken(server)
    .then((userContext) => {
      userToken = userContext.token
      utils.getInitiatives()
      .then(data => {
        initiatives = data
        initiativeId = data[0].id
        done()
      })
    })
  })

  describe(`POST`, () => {
    it('create a new', (done) => {
      const options = {
        method: 'POST',
        url: ENDPOINT,
        headers: {
          'Authorization': `Bearer ${userToken}`
        },
        payload: {
          initiativeId: initiativeId
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        expect(response.result).to.exist()
        expect(response.result.initiativeId).to.exists()
        utils.getUser()
        .then(user => {
          expect(user.favorites).to.equals(0)
          done()
        })
      })
    })

    it('try favorite again same initiative', (done) => {
      const options = {
        method: 'POST',
        url: ENDPOINT,
        headers: {
          'Authorization': `Bearer ${userToken}`
        },
        payload: {
          initiativeId: initiativeId
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(422)
        done()
      })
    })

    it('full list favorite', (done) => {
      const options = {
        method: 'POST',
        url: ENDPOINT,
        headers: {
          'Authorization': `Bearer ${userToken}`
        },
        payload: {
          initiativeId: initiatives[1].id
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(422)
        done()
      })
    })

    it('not send required data', (done) => {
      const options = {
        method: 'POST',
        url: ENDPOINT,
        headers: {
          'Authorization': `Bearer ${userToken}`
        },
        payload: {}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(400)
        done()
      })
    })

    it('not found initiativeId', (done) => {
      const options = {
        method: 'POST',
        url: ENDPOINT,
        headers: {
          'Authorization': `Bearer ${userToken}`
        },
        payload: {
          initiativeId: '09919733-3d2f-4d7a-a9bd-ad6d4c3822e7'
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(422)
        done()
      })
    })
  })

  describe(`GET`, () => {
    it('get all', (done) => {
      const options = {
        method: 'GET',
        url: ENDPOINT,
        headers: {'Authorization': `Bearer ${userToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        expect(response.result).to.exist()
        expect(response.result.total).to.greaterThan(0)
        expect(response.result.skip).to.exist()
        expect(response.result.limit).to.exist()
        expect(response.result.items).to.not.be.empty()
        done()
      })
    })
  })
})
