'use strict'

const Handler = require('./media.handler')
const Schema = require('./media.schema')
const Helpers = require('../../core/lib/helpers')
const config = require('../../../config/env')

const routes = [
  {
    method: 'POST',
    path: '/initiatives/{initiativeId}/medias',
    config: {
      description: 'Post medias',
      notes: 'Upload one medias',
      tags: ['api', 'medias'],
      payload: {
        output: 'stream',
        parse: true,
        allow: 'multipart/form-data',
        maxBytes: config.fileSize
      },
      auth: {
        scope: ['admin']
      },
      handler: Handler.create,
      validate: {
        payload: Schema.create,
        params: {initiativeId: Helpers.Joi.validate.uuidv4}
      }
    }
  },
  {
    method: 'GET',
    path: '/initiatives/{initiativeId}/medias',
    config: {
      description: 'GET medias',
      notes: 'Get all medias',
      tags: ['api', 'medias'],
      auth: false,
      handler: Handler.list,
      validate: {
        params: {initiativeId: Helpers.Joi.validate.uuidv4},
        query: Schema.query
      },
      response: {
        schema: Helpers.Joi.validate.pagination.response
      }
    }
  },
  {
    method: 'GET',
    path: '/initiatives/{initiativeId}/medias/{id}',
    config: {
      description: 'GET one media',
      notes: 'Get one media',
      tags: ['api', 'medias'],
      auth: false,
      handler: Handler.read,
      validate: {
        params: {
          id: Helpers.Joi.validate.uuidv4,
          initiativeId: Helpers.Joi.validate.uuidv4
        }
      }
    }
  },
  {
    method: 'POST',
    path: '/initiatives/{initiativeId}/medias/{id}/setcover',
    config: {
      description: 'set initiative cover',
      tags: ['api', 'medias'],
      auth: {
        scope: ['admin']
      },
      handler: Handler.setCover,
      validate: {
        params: {
          id: Helpers.Joi.validate.uuidv4,
          initiativeId: Helpers.Joi.validate.uuidv4
        }
      }
    }
  },
  {
    method: 'PATCH',
    path: '/initiatives/{initiativeId}/medias/{id}',
    config: {
      description: 'Update media caption',
      tags: ['api', 'medias'],
      auth: {
        scope: ['admin']
      },
      handler: Handler.update,
      validate: {
        params: {
          id: Helpers.Joi.validate.uuidv4,
          initiativeId: Helpers.Joi.validate.uuidv4
        },
        payload: Schema.update
      }
    }
  },
  {
    method: 'DELETE',
    path: '/initiatives/{initiativeId}/medias/{id}',
    config: {
      description: 'Remove one media',
      notes: 'Remove one media',
      tags: ['api', 'medias'],
      auth: {
        scope: ['admin']
      },
      handler: Handler.destroy,
      validate: {
        params: {
          id: Helpers.Joi.validate.uuidv4,
          initiativeId: Helpers.Joi.validate.uuidv4
        }
      }
    }
  }
]

if (config.env === 'development') {
  routes.push({
    method: 'GET',
    path: '/static/{param*}',
    config: {
      auth: false
    },
    handler: {
      directory: {
        path: 'uploads'
      }
    }
  })
}

module.exports = routes
