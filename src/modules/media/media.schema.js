'use strict'

const Joi = require('joi')
const Helpers = require('../../core/lib/helpers')

const schema = {
  caption: Joi.string().max(50),
  file: Joi.any().meta({ swaggerType: 'file' })
}

const create = Joi.object({
  caption: schema.caption.optional(),
  file: schema.file.required()
})

const update = Joi.object({
  caption: schema.caption.required()
})

const query = Helpers.Joi.validate.pagination.query.concat(
  Joi.object({
    type: Joi.only(['image', 'file'])
  })
)

module.exports = {
  create, update, query
}
