'use strict'

const models = require('../../models')
const fileUploadService = require('../../services/fileUpload')

module.exports = {
  create, list, read, setCover, update, destroy
}

async function create (request, reply) {
  try {
    const id = request.params.initiativeId
    const initiative = await models.Initiative.findById(id)

    if (!initiative) {
      return reply.notFound()
    }

    const opts = {
      model: 'Initiative',
      id: id
    }

    const data = await fileUploadService.upload(request.payload, opts)

    data.initiativeId = id

    if (request.payload.caption) {
      data.caption = request.payload.caption
    }

    const result = await models.Media.create(data)

    reply(result)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function list (request, reply) {
  try {
    const options = {
      limit: request.query.limit,
      offset: request.query.skip,
      where: {
        initiativeId: request.params.initiativeId
      }
    }

    if (request.query.type) {
      options.where.mediaType = request.query.type
    }

    const result = await models.Media.findAndCountAll(options)

    const response = {
      total: result.count,
      skip: request.query.skip,
      limit: request.query.limit,
      items: result.rows
    }
    reply(response)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function read (request, reply) {
  try {
    const options = {
      where: {
        id: request.params.id,
        initiativeId: request.params.initiativeId
      }
    }
    const result = await models.Media.findOne(options)
    if (!result) {
      return reply.notFound()
    }
    reply(result)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function setCover (request, reply) {
  try {
    const initiativeId = request.params.initiativeId

    const options = {
      where: {
        id: request.params.id,
        initiativeId: initiativeId
      }
    }

    const media = await models.Media.findOne(options)

    if (!media) {
      return reply.notFound()
    }

    const initiative = await models.Initiative.findById(initiativeId)

    await initiative.setCoverImage(media)

    reply({message: 'Ok'})
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function update (request, reply) {
  try {
    const options = {
      where: {
        id: request.params.id,
        initiativeId: request.params.initiativeId
      }
    }
    const result = await models.Media.update(request.payload, options)
    if (result[0] === 0) {
      return reply.notFound()
    }
    const updated = await models.Media.findById(request.params.id)
    reply(updated)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function destroy (request, reply) {
  try {
    const options = {
      where: {
        id: request.params.id,
        initiativeId: request.params.initiativeId
      }
    }

    const media = await models.Media.findOne(options)

    if (!media) {
      return reply.notFound()
    }

    fileUploadService.remove(media)

    await media.destroy()

    reply({message: 'OK'})
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}
