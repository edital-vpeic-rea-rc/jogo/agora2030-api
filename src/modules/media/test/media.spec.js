/* global describe, before, after, it, expect, server */

const utils = require('../../../test/utils')

let initiativeId = null
let ENDPOINT = null

describe(`#Medias`, () => {
  let adminToken = null
  let id = null
  before((done) => {
    utils.getToken(server, 'admin')
    .then((adminContext) => {
      adminToken = adminContext.token
      utils.getInitiative()
      .then(initiative => {
        initiativeId = initiative
        ENDPOINT = `/v1/initiatives/${initiativeId}/medias`
        done()
      })
    })
  })

  after((done) => {
    const exec = require('child_process').exec
    exec('rm -rf src/test/uploads', done)
  })

  describe(`POST`, () => {
    it('create a new', (done) => {
      let payload = [
        '------WebKitFormBoundaryS4AeNzAzUP7OArMi',
        'Content-Disposition: form-data; name="file"; filename="CIMG3456.JPG"',
        'Content-Type: image/jpeg',
        '',
        '',
        '------WebKitFormBoundaryS4AeNzAzUP7OArMi--'
      ]
      payload = payload.join('\r\n')
      const options = {
        method: 'POST',
        url: ENDPOINT,
        headers: {
          'Authorization': `Bearer ${adminToken}`,
          'content-Type': 'multipart/form-data; boundary=----WebKitFormBoundaryS4AeNzAzUP7OArMi'
        },
        payload: payload
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        expect(response.result).to.exist()
        expect(response.result.originalFileName).to.exists()
        expect(response.result.extName).to.exists()
        expect(response.result.contentType).to.exists()
        expect(response.result.mediaType).to.exists()
        expect(response.result.fileName).to.exists()
        expect(response.result.folderPath).to.exists()
        id = response.result.id
        done()
      })
    })
  })

  describe(`GET`, () => {
    it('get all', (done) => {
      const options = {
        method: 'GET',
        url: ENDPOINT,
        headers: {'Authorization': `Bearer ${adminToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        expect(response.result).to.exist()
        expect(response.result.total).to.greaterThan(0)
        expect(response.result.skip).to.exist()
        expect(response.result.limit).to.exist()
        expect(response.result.items).to.not.be.empty()
        done()
      })
    })
    it('filter type', (done) => {
      const options = {
        method: 'GET',
        url: `${ENDPOINT}?type=image`,
        headers: {'Authorization': `Bearer ${adminToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        expect(response.result).to.exist()
        expect(response.result.total).to.greaterThan(0)
        expect(response.result.skip).to.exist()
        expect(response.result.limit).to.exist()
        expect(response.result.items).to.not.be.empty()
        done()
      })
    })
    it('filter invalid type', (done) => {
      const options = {
        method: 'GET',
        url: `${ENDPOINT}?type=invalid`,
        headers: {'Authorization': `Bearer ${adminToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(400)
        done()
      })
    })
  })

  describe(`GET /{id}`, () => {
    it('get one', (done) => {
      const options = {
        method: 'GET',
        url: `${ENDPOINT}/${id}`,
        headers: {'Authorization': `Bearer ${adminToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        expect(response.result).to.exist()
        expect(response.result.id).to.equals(id)
        done()
      })
    })

    it('not found', (done) => {
      const options = {
        method: 'GET',
        url: `${ENDPOINT}/09919733-3d2f-4d7a-a9bd-ad6d4c3822e7`,
        headers: {'Authorization': `Bearer ${adminToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(404)
        done()
      })
    })

    it('invalid id', (done) => {
      const options = {
        method: 'GET',
        url: `${ENDPOINT}/invalid`,
        headers: {'Authorization': `Bearer ${adminToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(400)
        done()
      })
    })
  })

  describe(`PATCH /{id}`, () => {
    it('update caption', (done) => {
      const options = {
        method: 'PATCH',
        url: `${ENDPOINT}/${id}`,
        headers: {'Authorization': `Bearer ${adminToken}`},
        payload: {
          caption: 'caption updated'
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        expect(response.result).to.exist()
        expect(response.result.caption).to.equals('caption updated')
        done()
      })
    })
  })
})
