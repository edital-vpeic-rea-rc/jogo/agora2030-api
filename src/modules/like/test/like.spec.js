/* global describe, before, it, expect, server */

const utils = require('../../../test/utils')

let initiativeId = null
let ENDPOINT = null

describe(`#Like`, () => {
  let userToken = null

  before((done) => {
    utils.getToken(server)
    .then((userContext) => {
      userToken = userContext.token
      utils.getInitiative()
      .then(initiative => {
        initiativeId = initiative
        ENDPOINT = `/v1/initiatives/${initiativeId}/likes`
        done()
      })
    })
  })

  describe(`POST`, () => {
    it('create a new', (done) => {
      const options = {
        method: 'POST',
        url: ENDPOINT,
        headers: {
          'Authorization': `Bearer ${userToken}`
        },
        payload: {
          terms: ['Replicável', 'Inovadora']
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        expect(response.result).to.exist()
        expect(response.result.terms).to.exists()
        done()
      })
    })

    it('not send required data', (done) => {
      const options = {
        method: 'POST',
        url: ENDPOINT,
        headers: {
          'Authorization': `Bearer ${userToken}`
        },
        payload: {
          terms: []
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(400)
        done()
      })
    })

    it('not found initiativeId', (done) => {
      const options = {
        method: 'POST',
        url: `/v1/initiatives/09919733-3d2f-4d7a-a9bd-ad6d4c3822e7/likes`,
        headers: {
          'Authorization': `Bearer ${userToken}`
        },
        payload: {
          terms: ['Replicável']
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(422)
        done()
      })
    })
  })

  describe(`GET`, () => {
    it('get all', (done) => {
      const options = {
        method: 'GET',
        url: ENDPOINT,
        headers: {'Authorization': `Bearer ${userToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        expect(response.result).to.exist()
        expect(response.result.total).to.greaterThan(0)
        expect(response.result.skip).to.exist()
        expect(response.result.limit).to.exist()
        expect(response.result.items).to.not.be.empty()
        done()
      })
    })

    it('not found initiativeId', (done) => {
      const options = {
        method: 'GET',
        url: `/v1/initiatives/09919733-3d2f-4d7a-a9bd-ad6d4c3822e7/likes`,
        headers: {'Authorization': `Bearer ${userToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        expect(response.result).to.exist()
        expect(response.result.total).to.equals(0)
        expect(response.result.skip).to.exist()
        expect(response.result.limit).to.exist()
        expect(response.result.items).to.be.empty()
        done()
      })
    })
  })
})
