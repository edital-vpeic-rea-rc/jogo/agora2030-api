'use strict'

const models = require('../../models')
const trace = require('../../services/trace')

module.exports = {
  list, create
}

async function create (request, reply) {
  try {
    request.payload.initiativeId = request.params.initiativeId
    request.payload.userId = request.auth.credentials.id

    const result = await models.Like.create(request.payload)

    trace({
      type: 'actionLike',
      userId: request.auth.credentials.id,
      initiativeId: request.params.initiativeId
    })

    reply(result)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function list (request, reply) {
  try {
    const options = {
      limit: request.query.limit,
      offset: request.query.skip,
      where: {
        initiativeId: request.params.initiativeId
      },
      distinct: true,
      include: [{
        model: models.User,
        as: 'user',
        attributes: ['id', 'firstName']
      }]
    }
    const result = await models.Like.findAndCountAll(options)
    const response = {
      total: result.count,
      skip: request.query.skip,
      limit: request.query.limit,
      items: result.rows
    }
    reply(response)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}
