'use strict'

const Handler = require('./like.handler')
const Schema = require('./like.schema')
const Helpers = require('../../core/lib/helpers')

module.exports = [
  {
    method: 'GET',
    path: '/initiatives/{initiativeId}/likes',
    config: {
      description: 'GET likes',
      notes: 'Get all likes',
      tags: ['api', 'likes'],
      auth: {
        scope: ['admin', 'user']
      },
      handler: Handler.list,
      validate: {
        query: Helpers.Joi.validate.pagination.query,
        params: {initiativeId: Helpers.Joi.validate.uuidv4}
      },
      response: {
        schema: Helpers.Joi.validate.pagination.response
      }
    }
  },
  {
    method: 'POST',
    path: '/initiatives/{initiativeId}/likes',
    config: {
      description: 'create like',
      tags: ['api', 'likes'],
      auth: {
        scope: ['admin', 'user']
      },
      handler: Handler.create,
      validate: {
        payload: Schema.create,
        params: {initiativeId: Helpers.Joi.validate.uuidv4}
      }
    }
  }
]
