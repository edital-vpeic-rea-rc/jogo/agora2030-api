'use strict'

const Joi = require('joi')

const schema = {
  terms: Joi.array().items(Joi.only(['Relevante', 'Replicável', 'Inovadora', 'Participativa', 'Criativa', 'Fascinante']))
}

const create = Joi.object({
  terms: schema.terms.min(1).required()
})

module.exports = {
  create
}
