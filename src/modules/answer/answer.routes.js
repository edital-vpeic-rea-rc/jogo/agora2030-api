'use strict'

const Handler = require('./answer.handler')
const Schema = require('./answer.schema')
const Helpers = require('../../core/lib/helpers')

module.exports = [
  {
    method: 'GET',
    path: '/initiatives/{initiativeId}/answers',
    config: {
      description: 'GET answers',
      notes: 'Get all answers',
      tags: ['api', 'answers'],
      auth: false,
      handler: Handler.list,
      validate: {
        query: Schema.query,
        params: {initiativeId: Helpers.Joi.validate.uuidv4}
      },
      response: {
        schema: Helpers.Joi.validate.pagination.response
      }
    }
  },
  {
    method: 'POST',
    path: '/initiatives/{initiativeId}/answers',
    config: {
      description: 'create answer',
      tags: ['api', 'answers'],
      auth: {
        scope: ['admin']
      },
      handler: Handler.create,
      validate: {
        payload: Schema.create,
        params: {initiativeId: Helpers.Joi.validate.uuidv4}
      }
    }
  },
  {
    method: 'GET',
    path: '/initiatives/{initiativeId}/answers/{id}',
    config: {
      description: 'GET one answer',
      notes: 'Get one answer',
      tags: ['api', 'answers'],
      auth: {
        scope: ['admin', 'user']
      },
      handler: Handler.read,
      validate: {
        params: {
          id: Helpers.Joi.validate.uuidv4,
          initiativeId: Helpers.Joi.validate.uuidv4
        }
      }
    }
  },
  {
    method: 'PATCH',
    path: '/initiatives/{initiativeId}/answers/{id}',
    config: {
      description: 'Update one answer',
      notes: 'Update one answer',
      tags: ['api', 'answers'],
      auth: {
        scope: ['admin']
      },
      handler: Handler.update,
      validate: {
        params: {
          id: Helpers.Joi.validate.uuidv4,
          initiativeId: Helpers.Joi.validate.uuidv4
        },
        payload: Schema.update
      }
    }
  },
  {
    method: 'DELETE',
    path: '/initiatives/{initiativeId}/answers/{id}',
    config: {
      description: 'Delete one answer',
      notes: 'Delete one answer',
      tags: ['api', 'answers'],
      auth: {
        scope: ['admin']
      },
      handler: Handler.destroy,
      validate: {
        params: {
          id: Helpers.Joi.validate.uuidv4,
          initiativeId: Helpers.Joi.validate.uuidv4
        }
      }
    }
  }
]
