/* global describe, before, it, expect, server */

const utils = require('../../../test/utils')
const models = require('../../../models')

let initiativeId = null
let ENDPOINT = null
let questionId = null

describe(`#Answer`, () => {
  let adminToken = null
  let id = null
  before((done) => {
    utils.getToken(server, 'admin')
    .then((adminContext) => {
      adminToken = adminContext.token
      utils.getInitiative()
      .then(initiative => {
        initiativeId = initiative
        ENDPOINT = `/v1/initiatives/${initiativeId}/answers`
        done()
      })
    })
  })

  describe(`POST`, () => {
    it('create a new', (done) => {
      models.Question.create({
        title: 'Question',
        help: 'Help to question',
        order: 1
      })
      .then(question => {
        questionId = question.id
        const options = {
          method: 'POST',
          url: ENDPOINT,
          headers: {
            'Authorization': `Bearer ${adminToken}`
          },
          payload: {
            text: 'Answer to question',
            questionId: questionId
          }
        }
        server.inject(options, (response) => {
          expect(response.statusCode).to.equals(200)
          expect(response.result).to.exist()
          expect(response.result.text).to.exists()
          id = response.result.id
          done()
        })
      })
    })

    it('not send required data', (done) => {
      const options = {
        method: 'POST',
        url: ENDPOINT,
        headers: {
          'Authorization': `Bearer ${adminToken}`
        },
        payload: {
          text: 'Answer to question'
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(400)
        done()
      })
    })
  })

  describe(`GET`, () => {
    it('get all', (done) => {
      const options = {
        method: 'GET',
        url: ENDPOINT,
        headers: {'Authorization': `Bearer ${adminToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        expect(response.result).to.exist()
        expect(response.result.total).to.greaterThan(0)
        expect(response.result.skip).to.exist()
        expect(response.result.limit).to.exist()
        expect(response.result.items).to.not.be.empty()
        done()
      })
    })
  })

  describe(`GET /{id}`, () => {
    it('get one by id', (done) => {
      const options = {
        method: 'GET',
        url: `${ENDPOINT}/${id}`,
        headers: {'Authorization': `Bearer ${adminToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        expect(response.result).to.exist()
        expect(response.result.id).to.equals(id)
        done()
      })
    })

    it('not found', (done) => {
      const options = {
        method: 'GET',
        url: `${ENDPOINT}/09919733-3d2f-4d7a-a9bd-ad6d4c3822e7`,
        headers: {'Authorization': `Bearer ${adminToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(404)
        done()
      })
    })

    it('invalid id', (done) => {
      const options = {
        method: 'GET',
        url: `${ENDPOINT}/invalid`,
        headers: {'Authorization': `Bearer ${adminToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(400)
        done()
      })
    })
  })

  describe(`PATCH /{id}`, () => {
    it('update one', (done) => {
      const options = {
        method: 'PATCH',
        url: `${ENDPOINT}/${id}`,
        headers: {'Authorization': `Bearer ${adminToken}`},
        payload: {
          text: 'text updated'
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        expect(response.result).to.exist()
        expect(response.result.id).to.equals(id)
        expect(response.result.text).to.equals('text updated')
        done()
      })
    })

    it('not found', (done) => {
      const options = {
        method: 'PATCH',
        url: `${ENDPOINT}/09919733-3d2f-4d7a-a9bd-ad6d4c3822e7`,
        headers: {'Authorization': `Bearer ${adminToken}`},
        payload: {
          text: 'text updated'
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(404)
        done()
      })
    })

    it('invalid id', (done) => {
      const options = {
        method: 'PATCH',
        url: `${ENDPOINT}/invalid`,
        headers: {'Authorization': `Bearer ${adminToken}`},
        payload: {
          text: 'text updated'
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(400)
        done()
      })
    })
  })

  describe(`DELETE /{id}`, () => {
    it('delete one', (done) => {
      const options = {
        method: 'DELETE',
        url: `${ENDPOINT}/${id}`,
        headers: {'Authorization': `Bearer ${adminToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        done()
      })
    })

    it('not found', (done) => {
      const options = {
        method: 'DELETE',
        url: `${ENDPOINT}/09919733-3d2f-4d7a-a9bd-ad6d4c3822e7`,
        headers: {'Authorization': `Bearer ${adminToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(404)
        done()
      })
    })

    it('invalid id', (done) => {
      const options = {
        method: 'DELETE',
        url: `${ENDPOINT}/invalid`,
        headers: {'Authorization': `Bearer ${adminToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(400)
        done()
      })
    })
  })
})
