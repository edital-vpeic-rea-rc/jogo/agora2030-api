'use strict'

const models = require('../../models')

module.exports = {
  list, create, read, update, destroy
}

async function list (request, reply) {
  try {
    let whereQuestionOpts = {}
    if (request.query.mobile !== undefined) {
      whereQuestionOpts.mobile = request.query.mobile
    }
    const options = {
      limit: request.query.limit,
      offset: request.query.skip,
      order: [['question', 'order', 'ASC']],
      where: {
        initiativeId: request.params.initiativeId
      },
      distinct: true,
      include: [{
        where: whereQuestionOpts,
        model: models.Question,
        as: 'question',
        attributes: ['id', 'title', 'order', 'mobile']
      }]
    }
    const result = await models.Answer.findAndCountAll(options)
    const response = {
      total: result.count,
      skip: request.query.skip,
      limit: request.query.limit,
      items: result.rows
    }
    reply(response)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function create (request, reply) {
  try {
    request.payload.initiativeId = request.params.initiativeId
    const result = await models.Answer.create(request.payload)
    reply(result)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function read (request, reply) {
  try {
    const options = {
      where: {
        id: request.params.id,
        initiativeId: request.params.initiativeId
      }
    }
    const result = await models.Answer.findOne(options)
    if (!result) {
      return reply.notFound()
    }
    reply(result)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function update (request, reply) {
  try {
    const options = {
      where: {
        id: request.params.id,
        initiativeId: request.params.initiativeId
      }
    }
    const result = await models.Answer.update(request.payload, options)
    if (result[0] === 0) {
      return reply.notFound()
    }
    const updated = await models.Answer.findById(request.params.id)
    reply(updated)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function destroy (request, reply) {
  try {
    const options = {
      where: {
        id: request.params.id,
        initiativeId: request.params.initiativeId
      }
    }
    const result = await models.Answer.destroy(options)
    if (!result) {
      return reply.notFound()
    }
    reply({message: 'OK'})
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}
