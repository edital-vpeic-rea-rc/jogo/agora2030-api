const Handler = require('./report.handler')
// const Schema = require('./report.schema')

module.exports = [
  {
    method: 'GET',
    path: '/reports/users',
    config: {
      description: 'GET report',
      tags: ['api', 'user', 'report'],
      auth: {
        scope: ['admin']
      },
      handler: Handler.usersReport
    }
  }
]
