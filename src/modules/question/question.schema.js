'use strict'

const Joi = require('joi')

const schema = {
  title: Joi.string().max(150),
  help: Joi.string().max(200).allow(''),
  active: Joi.boolean(),
  mobile: Joi.boolean(),
  order: Joi.number().integer()
}

const create = Joi.object({
  title: schema.title.required(),
  help: schema.help.optional(),
  active: schema.active.optional(),
  mobile: schema.mobile.optional(),
  order: schema.order.optional()
})

const update = Joi.object({
  title: schema.title.optional(),
  help: schema.help.optional(),
  active: schema.active.optional(),
  mobile: schema.mobile.optional(),
  order: schema.order.optional()
}).required().min(1)

module.exports = {
  create, update
}
