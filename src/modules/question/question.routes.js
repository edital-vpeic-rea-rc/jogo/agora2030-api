'use strict'

const Handler = require('./question.handler')
const Schema = require('./question.schema')
const Helpers = require('../../core/lib/helpers')

module.exports = [
  {
    method: 'GET',
    path: '/questions',
    config: {
      description: 'GET questions',
      notes: 'Get all questions',
      tags: ['api', 'questions'],
      auth: {
        scope: ['admin', 'user']
      },
      handler: Handler.list,
      validate: {
        query: Helpers.Joi.validate.pagination.query
      },
      response: {
        schema: Helpers.Joi.validate.pagination.response
      }
    }
  },
  {
    method: 'POST',
    path: '/questions',
    config: {
      description: 'create one questions',
      tags: ['api', 'questions'],
      auth: {
        scope: ['admin']
      },
      handler: Handler.create,
      validate: {
        payload: Schema.create
      }
    }
  },
  {
    method: 'GET',
    path: '/questions/{id}',
    config: {
      description: 'GET one questions',
      notes: 'Get one questions by id',
      tags: ['api', 'user'],
      auth: {
        scope: ['admin', 'user']
      },
      handler: Handler.read,
      validate: {
        params: {id: Helpers.Joi.validate.uuidv4}
      }
    }
  },
  {
    method: 'PATCH',
    path: '/questions/{id}',
    config: {
      description: 'Update one questions',
      tags: ['api', 'questions'],
      auth: {
        scope: ['admin']
      },
      handler: Handler.update,
      validate: {
        payload: Schema.update,
        params: {id: Helpers.Joi.validate.uuidv4}
      }
    }
  },
  {
    method: 'GET',
    path: '/initiatives/{initiativeId}/questions/avaliables',
    config: {
      description: 'Get no answerd questions',
      tags: ['api', 'questions'],
      auth: {
        scope: ['admin']
      },
      handler: Handler.availables,
      validate: {
        params: {initiativeId: Helpers.Joi.validate.uuidv4}
      }
    }
  }

]
