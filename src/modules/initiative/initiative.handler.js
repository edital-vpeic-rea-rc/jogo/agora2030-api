'use strict'

const models = require('../../models')
const trace = require('../../services/trace')
const Op = models.Sequelize.Op

module.exports = {
  create,
  list,
  read,
  listAgora,
  readAgora,
  update,
  addTag,
  removeTag,
  addOds,
  removeOds,
  updatePublishStatus
}

async function create (request, reply) {
  try {
    const initiative = await models.Initiative.create(request.payload)

    models.Tag.initiativeTagsCreate(initiative, request.payload.tags)
    .then(initiative.setOds(request.payload.ods))

    reply(initiative)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

// Usado para a landing
async function list (request, reply) {
  try {
    let where = {published: true}
    let odsWhere = {}
    let filterAction = 'actionView'

    if (request.query.filter) {
      if (request.query.filter.q) {
        where.title = {
          [Op.like]: `%${request.query.filter.q}%`
        }
      }
      if (request.query.filter.ods) {
        odsWhere.id = request.query.filter.ods
      }

      if (request.query.filter.actionType) {
        filterAction = request.query.filter.actionType
      }
    }

    const options = {
      limit: request.query.limit,
      offset: request.query.skip,
      distinct: true,
      attributes: [
        'id', 'title', 'slug', 'addressLatitude', 'addressLongitude', 'publishedAt',
        [models.sequelize.literal(`(SELECT COUNT(*) FROM traces WHERE traces.initiativeId = Initiative.id and traces.type = "${filterAction}")`), 'filterToOrder'],
        [models.sequelize.literal(`(SELECT COUNT(*) FROM traces WHERE traces.initiativeId = Initiative.id and traces.type = "actionLike")`), 'actionLikeCount'],
        [models.sequelize.literal(`(SELECT COUNT(*) FROM traces WHERE traces.initiativeId = Initiative.id and traces.type = "actionView")`), 'actionViewCount'],
        [models.sequelize.literal(`(SELECT COUNT(*) FROM traces WHERE traces.initiativeId = Initiative.id and traces.type = "actionShare")`), 'actionShareCount'],
        [models.sequelize.literal(`(SELECT COUNT(*) FROM traces WHERE traces.initiativeId = Initiative.id and traces.type = "actionDonate")`), 'actionDonateCount'],
        [models.sequelize.literal(`(SELECT COUNT(*) FROM traces WHERE traces.initiativeId = Initiative.id and traces.type = "actionFavorite")`), 'actionFavoriteCount'],
        [models.sequelize.literal(`(SELECT COUNT(*) FROM traces WHERE traces.initiativeId = Initiative.id and traces.type = "actionComment")`), 'actionCommentCount']
      ],
      where: where,
      include: [
        {
          association: 'coverImage'
        },
        {
          where: odsWhere,
          association: 'ods',
          attributes: ['id', 'number', 'color', 'title'],
          through: {attributes: []}
        },
        {
          association: 'tags',
          attributes: ['id', 'text'],
          through: {attributes: []}
        }
      ],
      order: [[models.sequelize.literal('filterToOrder'), 'DESC']]
    }

    if (odsWhere.id) {
      delete options.limit
    }

    const result = await models.Initiative.findAndCountAll(options)

    const response = {
      total: result.count,
      skip: request.query.skip,
      limit: request.query.limit,
      items: result.rows
    }

    reply(response)
  } catch (error) {
    console.log(error)
    reply.badImplementationCustom(error)
  }
}

async function listAgora (request, reply) {
  try {
    const userId = request.auth.credentials.id
    const scope = request.query.scope
    let where = {published: true}
    let odsWhere = {}

    if (request.query.filter) {
      if (request.query.filter.q) {
        where.title = {
          [Op.like]: `%${request.query.filter.q}%`
        }
      }
      if (request.query.filter.ods) {
        odsWhere.id = request.query.filter.ods
      }
    }

    if (scope === 'admin') {
      // Apenas para o ambiente de admin (não aplica a lógica do game)
      const options = {
        limit: request.query.limit,
        offset: request.query.skip,
        order: [['updatedAt', 'DESC']]
      }
      const initiatives = await models.Initiative.findAndCountAll(options)
      const res = {
        total: initiatives.count,
        skip: request.query.skip,
        limit: request.query.limit,
        items: initiatives.rows
      }
      return reply(res)
    }

    // consulta com os campos base para as listagens no app
    const result = await models.Initiative.findAndCountAll(
      {
        limit: request.query.limit,
        offset: request.query.skip,
        where: where,
        distinct: true,
        attributes: ['id', 'title'],
        include: [
          {
            association: 'coverImage'
          },
          {
            association: 'ods',
            where: odsWhere,
            attributes: ['id', 'number', 'color'],
            through: {attributes: []}
          },
          {
            association: 'trace',
            attributes: ['userId']
          }
        ]
      }
    )

    // Adiciona o campo visualized para indiciar se o usuário ja vizualizou a solução,
    // o numberOfActions contem a quantidade de ações que ja forão realizados na solução,
    // esses campos servirão para ordenação manual posteiomente
    // essa estratégia foi usada devido a um bug no Sequelize (Mysql) que não permite
    // fazer 'group' por apenas uma coluna (caso exista includes)
    const addVisualizedAndCountActions = result.rows.map(item => {
      const initiative = item.toJSON()

      const userIds = initiative.trace.map(trace => {
        return trace.userId
      })

      initiative.visualized = (userIds.indexOf(userId) > -1) ? 1 : 0
      initiative.numberOfActions = initiative.trace.length

      if (initiative.coverImage) {
        initiative.coverImage = initiative.coverImage.toJSON().urls
      }

      delete initiative.trace
      return initiative
    })

    // Ordena as iniciativas de acordo com o que vem no scope
    let addOrder = null

    if (scope === 'popular') {
      addOrder = addVisualizedAndCountActions.sort(function (a, b) {
        return b.numberOfActions - a.numberOfActions
      })
    } else { // Ordenação padrão (Primero as que o usuáro não visualizou e que tem poucas ações)
      addOrder = addVisualizedAndCountActions.sort(function (a, b) {
        return a.visualized - b.visualized || a.numberOfActions - b.numberOfActions
      })
    }

    const response = {
      total: result.count,
      skip: request.query.skip,
      limit: request.query.limit,
      items: addOrder
    }

    reply(response)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function readAgora (request, reply) {
  try {
    const scope = request.query.scope

    const result = await models.Initiative.findOne({
      where: {id: request.params.id},
      include: [
        {
          association: 'coverImage'
        },
        {
          association: 'ods',
          attributes: ['id', 'title', 'number', 'color', 'description'],
          through: {attributes: []}
        },
        {
          association: 'trace',
          attributes: ['userId', 'type'],
          include: [{
            association: 'user',
            attributes: ['id', 'firstName', 'avatarPath', 'socialAvatar', 'avatarExtName']
          }],
          required: false
        },
        {
          association: 'tags',
          attributes: ['id', 'text'],
          through: {attributes: []},
          required: false
        }
      ]
    })

    if (!result) {
      return reply.notFound()
    }

    if (scope === 'admin') {
      delete result.trace
      return reply(result)
    }

    let whoLikes = []
    let actions = []
    let whoLikesTotal = 0

    for (let i = 0; i < result.trace.length; i++) {
      let element = result.trace[i].toJSON()
      actions.push(`${element.userId}_${element.type}`)
      if (element.type === 'actionLike') {
        whoLikesTotal++
        if (whoLikes.length < 5 && element.user) {
          whoLikes.push(element.user.toJSON())
        }
      }
    }

    const whoLikesQnt = (whoLikesTotal - whoLikes.length <= 0) ? 0 : whoLikesTotal - whoLikes.length

    const userId = request.auth.credentials.id
    const initiative = result.toJSON()

    initiative.actions = {
      actionLike: (actions.indexOf(`${userId}_actionLike`) > -1),
      actionFavorite: (actions.indexOf(`${userId}_actionFavorite`) > -1),
      actionDonate: (actions.indexOf(`${userId}_actionDonate`) > -1),
      actionComment: (actions.indexOf(`${userId}_actionComment`) > -1),
      actionView: (actions.indexOf(`${userId}_actionView`) > -1),
      actionShare: (actions.indexOf(`${userId}_actionShare`) > -1)
    }

    initiative.whoLikes = whoLikes
    initiative.whoLikesQnt = whoLikesQnt

    delete initiative.trace

    await trace({
      type: 'actionView',
      userId: userId,
      initiativeId: request.params.id
    })

    reply(initiative)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function read (request, reply) {
  try {
    const result = await models.Initiative.findOne({
      where: {slug: request.params.slug},
      include: [
        {
          association: 'coverImage'
        },
        {
          association: 'ods',
          attributes: ['id', 'number', 'color', 'title', 'description'],
          through: {attributes: []}
        },
        {
          association: 'tags',
          attributes: ['id', 'text'],
          through: {attributes: []}
        }
      ]
    })
    if (!result) {
      return reply.notFound()
    }
    reply(result)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function update (request, reply) {
  try {
    const id = request.params.id
    const result = await models.Initiative.update(request.payload, {where: {id}, individualHooks: true})
    if (result[0] === 0) {
      return reply.notFound()
    }
    const updated = await models.Initiative.findById(id)
    reply(updated)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function addTag (request, reply) {
  try {
    const initiative = await models.Initiative.findById(request.params.initiativeId)

    if (!initiative) {
      return reply.notFound()
    }

    const tagData = {text: request.payload.tag.toLowerCase()}
    const options = {where: {text: tagData.text}, defaults: tagData}

    const tag = await models.Tag.findOrCreate(options)
    const tagCreated = await initiative.addTag(tag[0].id)

    let result = tagCreated[0][0].toJSON()
    result.text = tag[0].text

    reply(result)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function removeTag (request, reply) {
  try {
    const initiative = await models.Initiative.findById(request.params.initiativeId)

    if (!initiative) {
      return reply.notFound()
    }

    const removed = await initiative.removeTag(request.params.id)

    if (!removed) {
      return reply.notFound()
    }

    reply({message: 'OK'})
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function addOds (request, reply) {
  try {
    const initiative = await models.Initiative.findById(request.params.initiativeId)

    if (!initiative) {
      return reply.notFound()
    }

    const odsCreated = await initiative.addOds(request.payload.odsId)

    let result = odsCreated[0][0].toJSON()

    reply(result)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function removeOds (request, reply) {
  try {
    const initiative = await models.Initiative.findById(request.params.initiativeId)

    if (!initiative) {
      return reply.notFound()
    }

    const removed = await initiative.removeOds(request.params.id)

    if (!removed) {
      return reply.notFound()
    }

    reply({message: 'OK'})
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function updatePublishStatus (request, reply) {
  try {
    const id = request.params.id
    const data = {published: request.payload.status}
    data.publishedAt = (data.published) ? new Date() : null
    const result = await models.Initiative.update(data, {where: {id: id}, individualHooks: true})
    if (result[0] === 0) {
      return reply.notFound()
    }
    const updated = await models.Initiative.findById(id)
    reply(updated)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}
