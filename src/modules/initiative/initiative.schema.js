'use strict'

const Joi = require('joi')
const Helpers = require('../../core/lib/helpers')

const schema = {
  title: Joi.string().max(100),
  description: Joi.string().max(300),
  institutionName: Joi.string().max(100),
  contactName: Joi.string().max(100),
  contactEmail: Joi.string().email(),
  language: Joi.only(['Português', 'Inglês']),
  source: Joi.string().max(30).allow(''),
  addressFormatted: Joi.string(),
  addressLatitude: Joi.number().min(-180).max(180),
  addressLongitude: Joi.number().min(-180).max(180),
  addressCountry: Joi.string().max(50),
  addressCity: Joi.string().max(50),
  addressState: Joi.string().max(50),
  coverImageId: Helpers.Joi.validate.uuidv4,
  tags: Joi.array().items(Joi.string().max(25)).max(2),
  ods: Joi.array().items(Helpers.Joi.validate.uuidv4).max(3)
}

const create = Joi.object({
  title: schema.title.required(),
  description: schema.description.required(),
  institutionName: schema.institutionName.required(),
  contactName: schema.contactName.required(),
  contactEmail: schema.contactEmail.required(),
  language: schema.language.required(),
  source: schema.source.optional(),
  addressFormatted: schema.addressFormatted.required(),
  addressLatitude: schema.addressLatitude.required(),
  addressLongitude: schema.addressLongitude.required(),
  addressCountry: schema.addressCountry.required(),
  addressCity: schema.addressCity.required(),
  addressState: schema.addressState.required(),
  tags: schema.tags.required(),
  ods: schema.ods.required()
})

const update = Joi.object({
  title: schema.title.optional(),
  description: schema.description.optional(),
  institutionName: schema.institutionName.optional(),
  contactName: schema.contactName.optional(),
  contactEmail: schema.contactEmail.optional(),
  language: schema.language.optional(),
  source: schema.source.optional(),
  addressFormatted: schema.addressFormatted.optional(),
  addressLatitude: schema.addressLatitude.optional(),
  addressLongitude: schema.addressLongitude.optional(),
  addressCountry: schema.addressCountry.optional(),
  addressCity: schema.addressCity.optional(),
  addressState: schema.addressState.optional(),
  coverImageId: schema.coverImageId.optional()
}).required().min(1)

const addTag = Joi.object({
  tag: Joi.string().max(20).required()
})

const addOds = Joi.object({
  odsId: Helpers.Joi.validate.uuidv4.required()
})

const query = Helpers.Joi.validate.pagination.query.concat(
  Joi.object({
    scope: Joi.only(['popular', 'admin']),
    filter: Joi.object({
      q: Joi.string(),
      ods: Helpers.Joi.validate.uuidv4
    })
  })
)

const openQuery = Helpers.Joi.validate.pagination.query.concat(
  Joi.object({
    filter: Joi.object({
      q: Joi.string(),
      ods: Helpers.Joi.validate.uuidv4,
      actionType: Joi.only(['actionLike', 'actionView', 'actionShare', 'actionDonate', 'actionFavorite', 'actionComment'])
    })
  })
)

const slug = Joi.string().regex(/^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$/).options({
  language: {string: {regex: {base: 'Invalid slug format'}}}
})

const updatePublishStatus = Joi.object({
  status: Joi.boolean().required()
})

module.exports = {
  create, update, addTag, addOds, query, slug, updatePublishStatus, openQuery
}
