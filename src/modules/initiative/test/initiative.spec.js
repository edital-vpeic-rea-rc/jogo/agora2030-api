/* global describe, before, it, expect, server */

const utils = require('../../../test/utils')
const ENDPOINT = '/v1/initiatives'
const ENDPOINT_GET = '/v1/initiatives/agora2030'
const models = require('../../../models')

describe(`#Initiative`, () => {
  let userToken = null
  let adminToken = null
  let id = null
  let tagId = null
  let odsId = null

  before((done) => {
    utils.getToken(server)
    .then((userContext) => {
      userToken = userContext.token
      utils.getToken(server, 'admin')
      .then((adminContext) => {
        adminToken = adminContext.token
        done()
      })
    })
  })

  describe(`POST ${ENDPOINT}`, () => {
    it('create a new', (done) => {
      models.Ods.findAll()
      .then(ods => {
        const ods1 = ods[0].id
        const ods2 = ods[1].id
        const options = {
          method: 'POST',
          url: ENDPOINT,
          headers: {'Authorization': `Bearer ${adminToken}`},
          payload: {
            title: 'Cuidado de fumantes',
            description: 'Relato de tratamento inovador',
            institutionName: 'FIOCRUZ',
            contactName: 'Jairo Martins',
            contactEmail: 'jairo@mail.com',
            language: 'Português',
            addressFormatted: 'Rua Sergio 12',
            addressCountry: 'Brasil',
            addressLatitude: 1.2923423423,
            addressLongitude: -1.21223321,
            addressCity: 'Brasília',
            addressState: 'Distrito Federal',
            tags: ['Fumantes', 'Tratamento'],
            ods: [ods1, ods2]
          }
        }
        server.inject(options, (response) => {
          expect(response.statusCode).to.equals(200)
          expect(response.result).to.exist()
          expect(response.result.id).to.exist()
          expect(response.result.title).to.exist()
          expect(response.result.description).to.exist()
          expect(response.result.institutionName).to.exist()
          expect(response.result.contactName).to.exist()
          expect(response.result.contactEmail).to.exist()
          expect(response.result.language).to.exist()
          expect(response.result.addressFormatted).to.exist()
          expect(response.result.addressLatitude).to.exist()
          expect(response.result.addressLongitude).to.exist()
          expect(response.result.addressCity).to.exist()
          expect(response.result.addressState).to.exist()
          expect(response.result.slug).to.equals('cuidado-de-fumantes')
          expect(response.result.published).to.equals(false)
          id = response.result.id
          done()
        })
      })
    })

    it('403 - role', (done) => {
      const options = {
        method: 'POST',
        url: ENDPOINT,
        headers: {'Authorization': `Bearer ${userToken}`},
        payload: {
          title: 'Cuidado de fumantes',
          description: 'Relato de tratamento inovador',
          institutionName: 'FIOCRUZ',
          contactName: 'Jairo Martins',
          contactEmail: 'jairo@mail.com',
          language: 'Português',
          addressCountry: 'Brasil',
          addressFormatted: 'Rua Sergio 12',
          addressLatitude: 1.2923423423,
          addressLongitude: -1.21223321,
          addressCity: 'Brasília',
          addressState: 'Distrito Federal',
          tags: ['Fumantes', 'Tratamento']
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(403)
        done()
      })
    })

    it('not send required data', (done) => {
      const options = {
        method: 'POST',
        url: ENDPOINT,
        headers: {'Authorization': `Bearer ${adminToken}`},
        payload: {}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(400)
        done()
      })
    })
  })

  describe(`GET ${ENDPOINT_GET}/{id}`, () => {
    it('get one', (done) => {
      const options = {
        method: 'GET',
        url: `${ENDPOINT_GET}/${id}`,
        headers: {'Authorization': `Bearer ${userToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        expect(response.result).to.exist()
        expect(response.result.id).to.equals(id)
        done()
      })
    })

    it('not found', (done) => {
      const options = {
        method: 'GET',
        url: `${ENDPOINT_GET}/09919733-3d2f-4d7a-a9bd-ad6d4c3822e7`,
        headers: {'Authorization': `Bearer ${userToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(404)
        done()
      })
    })

    it('invalid id', (done) => {
      const options = {
        method: 'GET',
        url: `${ENDPOINT_GET}/invalid`,
        headers: {'Authorization': `Bearer ${userToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(400)
        done()
      })
    })
  })

  describe(`PATCH ${ENDPOINT}/{id}`, () => {
    it('update one', (done) => {
      const options = {
        method: 'PATCH',
        url: `${ENDPOINT}/${id}`,
        headers: {'Authorization': `Bearer ${adminToken}`},
        payload: {
          title: 'Title updated'
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        expect(response.result).to.exist()
        expect(response.result.id).to.equals(id)
        expect(response.result.title).to.equals('Title updated')
        expect(response.result.slug).to.equals('title-updated')
        expect(response.result.institutionName).to.equals('FIOCRUZ')
        done()
      })
    })
    it('id not found', (done) => {
      const options = {
        method: 'PATCH',
        url: `${ENDPOINT}/09919733-3d2f-4d7a-a9bd-ad6d4c3822e7`,
        headers: {'Authorization': `Bearer ${adminToken}`},
        payload: {
          title: 'Title updated'
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(404)
        done()
      })
    })
  })

  describe(`POST ${ENDPOINT}/{initiativeId}/tags`, () => {
    it('add a new tag', (done) => {
      const options = {
        method: 'POST',
        url: `${ENDPOINT}/${id}/tags`,
        headers: {'Authorization': `Bearer ${adminToken}`},
        payload: {
          tag: 'tagtest'
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        expect(response.result).to.exist()
        expect(response.result.id).to.exist()
        expect(response.result.initiativeId).to.equals(id)
        expect(response.result.text).to.equals('tagtest')
        tagId = response.result.tagId

        models.InitiativeTags.findOne({where: {initiativeId: id, tagId: tagId}})
        .then(body => {
          expect(body).to.exist()
          done()
        })
      })
    })

    it('invalid tag', (done) => {
      const options = {
        method: 'POST',
        url: `${ENDPOINT}/${id}/tags`,
        headers: {'Authorization': `Bearer ${adminToken}`},
        payload: {
          tag: 'tagtestmuitograndeparapassarnavalidacaolocadojoi'
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(400)
        done()
      })
    })

    it('initiative not found', (done) => {
      const options = {
        method: 'POST',
        url: `${ENDPOINT}/09919733-3d2f-4d7a-a9bd-ad6d4c3822e7/tags`,
        headers: {'Authorization': `Bearer ${adminToken}`},
        payload: {
          tag: 'tagtest'
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(404)
        done()
      })
    })
  })

  describe(`DELETE ${ENDPOINT}/{initiativeId}/tags/{id}`, () => {
    it('remove a tag', (done) => {
      const options = {
        method: 'DELETE',
        url: `${ENDPOINT}/${id}/tags/${tagId}`,
        headers: {'Authorization': `Bearer ${adminToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        models.InitiativeTags.findOne({where: {initiativeId: id, tagId: tagId}})
        .then(body => {
          expect(body).to.not.exist()
          done()
        })
      })
    })
    it('initiative not found', (done) => {
      const options = {
        method: 'DELETE',
        url: `${ENDPOINT}/09919733-3d2f-4d7a-a9bd-ad6d4c3822e7/tags/${tagId}`,
        headers: {'Authorization': `Bearer ${adminToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(404)
        done()
      })
    })
    it('tag not found', (done) => {
      const options = {
        method: 'DELETE',
        url: `${ENDPOINT}/${id}/tags/09919733-3d2f-4d7a-a9bd-ad6d4c3822e7`,
        headers: {'Authorization': `Bearer ${adminToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(404)
        done()
      })
    })
  })

  describe(`POST ${ENDPOINT}/{initiativeId}/ods`, () => {
    it('add a new ods', (done) => {
      models.Ods.findOne({where: {number: 10}})
      .then(ods => {
        odsId = ods.id
        const options = {
          method: 'POST',
          url: `${ENDPOINT}/${id}/ods`,
          headers: {'Authorization': `Bearer ${adminToken}`},
          payload: {
            odsId: odsId
          }
        }
        server.inject(options, (response) => {
          expect(response.statusCode).to.equals(200)
          expect(response.result).to.exist()
          expect(response.result.id).to.exist()
          expect(response.result.initiativeId).to.equals(id)
          models.InitiativeOds.findOne({where: {initiativeId: id, odsId: odsId}})
          .then(body => {
            expect(body).to.exist()
            done()
          })
        })
      })
    })

    it('invalid ods', (done) => {
      const options = {
        method: 'POST',
        url: `${ENDPOINT}/${id}/ods`,
        headers: {'Authorization': `Bearer ${adminToken}`},
        payload: {
          odsId: 'invalid'
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(400)
        done()
      })
    })

    it('initiative not found', (done) => {
      const options = {
        method: 'POST',
        url: `${ENDPOINT}/09919733-3d2f-4d7a-a9bd-ad6d4c3822e7/ods`,
        headers: {'Authorization': `Bearer ${adminToken}`},
        payload: {
          odsId: odsId
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(404)
        done()
      })
    })
  })

  describe(`DELETE ${ENDPOINT}/{initiativeId}/ods/{id}`, () => {
    it('remove a ods', (done) => {
      const options = {
        method: 'DELETE',
        url: `${ENDPOINT}/${id}/ods/${odsId}`,
        headers: {'Authorization': `Bearer ${adminToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        models.InitiativeOds.findOne({where: {initiativeId: id, odsId: odsId}})
        .then(body => {
          expect(body).to.not.exist()
          done()
        })
      })
    })
    it('initiative not found', (done) => {
      const options = {
        method: 'DELETE',
        url: `${ENDPOINT}/09919733-3d2f-4d7a-a9bd-ad6d4c3822e7/ods/${tagId}`,
        headers: {'Authorization': `Bearer ${adminToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(404)
        done()
      })
    })
    it('ods not found', (done) => {
      const options = {
        method: 'DELETE',
        url: `${ENDPOINT}/${id}/ods/09919733-3d2f-4d7a-a9bd-ad6d4c3822e7`,
        headers: {'Authorization': `Bearer ${adminToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(404)
        done()
      })
    })
  })

  describe(`POST ${ENDPOINT}/{id}/publish`, () => {
    it('publish', (done) => {
      const options = {
        method: 'POST',
        url: `${ENDPOINT}/${id}/publish`,
        headers: {'Authorization': `Bearer ${adminToken}`},
        payload: {
          status: true
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        expect(response.result).to.exist()
        expect(response.result.published).to.equals(true)
        expect(response.result.publishedAt).to.exist()
        done()
      })
    })
    it('unPublish', (done) => {
      const options = {
        method: 'POST',
        url: `${ENDPOINT}/${id}/publish`,
        headers: {'Authorization': `Bearer ${adminToken}`},
        payload: {
          status: false
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        expect(response.result).to.exist()
        expect(response.result.published).to.equals(false)
        expect(response.result.publishedAt).to.be.null()
        done()
      })
    })
    it('notfound', (done) => {
      const options = {
        method: 'POST',
        url: `${ENDPOINT}/09919733-3d2f-4d7a-a9bd-ad6d4c3822e7/publish`,
        headers: {'Authorization': `Bearer ${adminToken}`},
        payload: {
          status: true
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(404)
        done()
      })
    })
  })
})
