'use strict'

const jwt = require('hapi-auth-jwt2')
const jsonWebToken = require('jsonwebtoken')
const models = require('../../models')
const key = require('../../../config/env').jwtSecret

exports.register = (server, options, next) => {
  server.register(jwt, registerAuth)

  function registerAuth (err) {
    if (err) { return next(err) }

    server.auth.strategy('jwt', 'jwt', {
      key: key,
      validateFunc: validate,
      verifyOptions: {algorithms: [ 'HS256' ]}
    })

    server.auth.default({
      strategy: 'jwt',
      scope: ['user', 'admin']
    })

    return next()
  }

  function validate (decoded, request, cb) {
    const token = request.headers.authorization.replace('Bearer ', '')
    models.User.findOne({where: {id: decoded.id}})
    .then(user => {
      if (!user) {
        return cb(null, false)
      }
      jsonWebToken.verify(token, key, (err, decoded) => {
        if (err) {
          return cb(null, false)
        }
        return cb(null, true)
      })
    })
    .catch((errr) => cb(null, false))
  }
}

exports.register.attributes = {
  name: 'auth-jwt',
  version: '1.0.0'
}
