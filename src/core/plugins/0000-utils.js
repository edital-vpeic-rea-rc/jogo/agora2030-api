exports.register = (server, options, next) => {
  server.register({
    register: require('hapi-boom-decorators')
  }, (err) => {
    if (!err) {
      return next()
    }
  })
  if (process.env.NODE_ENV !== 'production') {
    server.register([require('vision'), require('inert'), { register: require('lout') }],
    (err) => {
      if (!err) {
        return next()
      }
    })
  }
}

exports.register.attributes = {
  name: 'utils',
  version: '1.0.0'
}
