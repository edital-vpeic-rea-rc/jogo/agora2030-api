'use strict'

exports.register = (server, options, next) => {
  server.register({
    register: require('hapi-router'),
    options: {
      routes: 'src/modules/**/*routes.js'
    }
  }, {routes: { prefix: '/v1' }}
  , (err) => {
    return next(err)
  })
}

exports.register.attributes = {
  name: 'routes',
  version: '1.0.0'
}
