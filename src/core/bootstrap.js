'use strict'

const server = require('./server')
const models = require('../models')
const path = require('path')
const Promise = require('bluebird')
const fs = Promise.promisifyAll(require('fs'))

module.exports = {start}

function start () {
  return Promise.resolve()
  .then(() => {
    console.log('--> Load plugins')
    const dir = path.join(__dirname, '/plugins')
    return fs.readdirAsync(dir)
    .map((file) => {
      return {
        register: require(path.join(dir, file))
      }
    })
    .then(registerToServer)
    .catch((err) => {
      console.log('--> Error load plugins', err)
      throw err
    })
  })
  .then(() => {
    console.log('--> Load database')
    if (process.env.NODE_ENV === 'test') {
      const seedTest = require('../../data/test/seed')
      return seedTest.init()
      .then(() => {
        return server
      })
    } else {
      return models.sequelize.sync()
      .then(() => {
        server.start((err) => {
          if (err) {
            throw err
          }
          console.log('info', 'Server running at: ' + server.info.uri)
        })
      })
      .catch((err) => {
        console.log('--> Error Load database', err)
        throw err
      })
    }
  })
  .catch((err) => {
    console.log('--> App Error' + err)
    throw err
  })
}

function registerToServer (plugins) {
  return new Promise((resolve, reject) => {
    server.register(plugins, (err) => {
      if (err) {
        return reject(err)
      }
      return resolve()
    })
  })
}
