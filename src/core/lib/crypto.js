'use strict'

const crypto = require('crypto')
const config = require('../../../config/env')

/**
 * Encripta dados utilizando o cipher camellia256
 * @param {String} data dados a serem encriptados
 * @param {String} encoding utf8, base64 ou hex
 * @param {String} output base64 ou hex
 * @return {String} O resultado da encriptação com o output selecionado
 */
exports.encrypt = (data, encoding = 'utf8', output = 'hex') => {
  const cipher = crypto.createCipher('camellia256', config.cipherPassword)
  const encrypted = cipher.update(data, encoding, output)

  return encrypted + cipher.final(output)
}

/**
 * Decripta dados utilizando o cipher camellia256
 * @param {String} data dados a serem decriptados
 * @param {String} encoding utf8, base64 ou hex
 * @param {String} output base64 ou hex
 * @return {String} O resultado da decriptação com o output selecionado
 */
exports.decrypt = (data, encoding = 'hex', output = 'utf8') => {
  const decipher = crypto.createDecipher('camellia256', config.cipherPassword)
  const decrypted = decipher.update(data, encoding, output)

  return decrypted + decipher.final(output)
}
