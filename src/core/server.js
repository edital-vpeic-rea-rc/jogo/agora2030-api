'use strict'

const Hapi = require('hapi')
const config = require('../../config/env')

let server = new Hapi.Server()

server.connection({
  host: config.server.host,
  port: config.server.port,
  routes: {
    cors: {
      origin: config.allowedDomains,
      credentials: true
    },
    validate: {
      options: {
        abortEarly: false
      }
    }
  }
})

module.exports = server
