const fs = require('fs-extra')
const uuidv1 = require('uuid/v1')
const path = require('path')
const config = require('../../config/env')
const Promise = require('bluebird')
const Boom = require('boom')
const sharp = require('sharp')

module.exports = {
  upload, remove, getFolderPath
}

function upload (payload, options) {
  return new Promise((resolve, reject) => {
    const file = payload.file

    if (!file) {
      return reject(Boom.badData('No file'))
    }

    const originalFileName = file.hapi.filename
    const contentType = file.hapi.headers['content-type']

    if (config.acceptMimes.indexOf(contentType) === -1) {
      return reject(Boom.badData(contentType + ' content-type not supported'))
    }

    const extName = path.extname(originalFileName)
    const mediaType = (contentType.split('/')[0] === 'image') ? 'image' : 'file'
    const fileName = uuidv1()
    const folderPath = getFolderPath(options, mediaType, fileName)
    const dir = config.uploadDir + folderPath

    fs.ensureDirSync(dir)

    const filePath = (mediaType === 'image') ? `${dir}original${extName}` : `${dir}${fileName}${extName}`

    const stream = fs.createWriteStream(filePath)

    stream.on('error', function (err) {
      return reject(err)
    })

    file.pipe(stream)

    file.on('end', function (err) {
      if (err) {
        return reject(err)
      }

      const fileData = {
        originalFileName,
        extName,
        contentType,
        mediaType,
        fileName,
        folderPath
      }

      if (config.env === 'test') {
        return resolve(fileData)
      }

      if (mediaType === 'image') {
        const sizesToResize = (options.model === 'Initiative') ? config.imageStyles : config.avatarStyles

        let promises = sizesToResize.map(style => {
          let resizePath = `${dir}${style.name}${extName}`
          return resizeImage(filePath, resizePath, style.width, style.heigth)
        })
        Promise.all(promises)
        .then(resized => {
          resolve(fileData)
        })
        .catch((errPromises) => {
          fs.removeSync(dir)
        })
      } else {
        resolve(fileData)
      }
    })
  })
}

function remove (options) {
  const mediaPath = (options.mediaType === 'file')
    ? `${config.uploadDir}${options.folderPath}${options.fileName}${options.extName}`
    : `${config.uploadDir}${options.folderPath}`
  fs.removeSync(mediaPath)
}

function getFolderPath (options, mediaType, fileName) {
  switch (options.model) {
    case 'Initiative':
      if (mediaType === 'file') {
        return `/initiatives/${options.id}/files/`
      }
      return `/initiatives/${options.id}/images/${fileName}/`
    case 'User':
      return `/users/${options.id}/avatar/`
    default:
      return `/temp/`
  }
}

function resizeImage (image, resizePath, width, heigth) {
  return sharp(image)
    .resize(width, heigth)
    .toFile(resizePath)
}
